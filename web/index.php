<?php
//Test Comment
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	/* special ajax here */
    define("AJAX_CALL",true);
}
require_once 'config/config.php';
require_once CONFIG."db.config.php";
@include_once CONFIG."custom.config.php";
require_once CONFIG."general.config.php";
if(DB_REQUIRED)
    require_once CLASSES."db.class.php";

require_once CLASSES."init.class.php";

if(defined("MAINTENANCE_MODE") && MAINTENANCE_MODE){
    echo "System is in Maintenance Mode";   
    return;
}

date_default_timezone_set(TIME_ZONE);

$init = new init();

if(isset($_GET['page']))
    define ('PAGE',strtolower($_GET['page']));
else
    define ('PAGE','default');
define('TITLE',ucfirst(strtolower(PAGE)));
/*

//Check for ETAG and Enable Caching as per that.
$etag = md5(TITLE.$init->session->getData("ROLE_CODE").PAGE);
$headers = getallheaders();

$LastChangeTime = 1428596461;//Just some value.

// Define the proxy or cache expire time 
$ExpireTime = 3600; // seconds (= one hour)

// Get request headers:
$headers = apache_request_headers();
// you could also use getallheaders() or $_SERVER
// or HTTP_SERVER_VARS 

// Add the content type of your page
header('Content-Type: text/html');

// Content language
header('Content-language: en');

// Set cache/proxy informations:
header('Cache-Control: max-age=' . $ExpireTime); // must-revalidate
header('Expires: '.gmdate('D, d M Y H:i:s', time()+$ExpireTime).' GMT');

// Set last modified (this helps search engines 
// and other web tools to determine if a page has
// been updated)
header('Last-Modified: '.gmdate('D, d M Y H:i:s', $LastChangeTime).' GMT');

// Send a "ETag" which represents the content
// (this helps browsers to determine if the page
// has been changed or if it can be loaded from
// the cache - this will speed up the page loading)
header('ETag: ' . $etag);

$DoIDsMatch = (isset($headers['If-None-Match']) and 
               preg_match("/".$etag."/", $headers['If-None-Match']));
if($DoIDsMatch){
    //echo $headers['If-None-Match'];
    //return;
    header('HTTP/1.1 304 Not Modified');
    
    // That's all, now close the connection:
    header('Connection: close');
    return;
}
*/

if(defined('DEBUG') && DEBUG)	var_dump($_GET);
if(defined('DEBUG') && DEBUG)	var_dump($_SESSION);
require_once 'init.php';
?>