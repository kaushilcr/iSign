<form class="form-horizontal" role="form" action="<?php echo BASE_URL;?>change-password.process" method="post" id="form">
    <div class="form-group required">
        <label class="col-sm-4 control-label">Current Password</label>
        <div class="col-sm-4 width45">
            <input type="password" class="form-control" name="cpwd" required <?php if($this->session->isAdmin()) echo "disabled readonly"; ?>>
        </div> 
    </div>
    <div class="form-group required">
        <label class="col-sm-4 control-label">New Password</label>
        <div class="col-sm-4 width45">
            <input type="password" class="form-control" name="pwd" required>
        </div> 
    </div>
    <div class="form-group required">
        <label class="col-sm-4 control-label">Confirm New Password</label>
        <div class="col-sm-4 width45">
            <input type="password" class="form-control" name="pwdc" required>
        </div> 
    </div>
    <div class="form-group required">

        <div class="col-sm-4 col-sm-offset-4">
            <button class="btn btn-success" type="submit">Change Password</button>
        </div> 
    </div>
</form>