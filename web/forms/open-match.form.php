<form action="<?php echo BASE_URL ?>admin/scorer-panel.html" class="form-horizontal" method="get">
    <!--<input type="text" class="form-control input-lg" name="match_id">-->
    
    <div class="form-group">
        <label for="match_id_text" class="col-sm-4 control-label">Select match</label>
        <div class="col-sm-6">
            <select name="match_id" id="" class="form-control" id="match_id_text">
               <?php
                    $sql = "SELECT a.`id`,b.`name` as 'team1' ,b.`short_name` as 'team1_short',c.`name` as 'team2',c.`short_name` as 'team2_short' FROM `match` a, `team` b, `team` c where a.`team1_id` = b.`id` and a.`team2_id` = c.`id` and `started`<>0";
                    $result = $this->db->query($sql);
                    if(!$result)
                        echo '<option value="">Error</option>';
                    else{
                        while($row = mysqli_fetch_array($result))
                        {
                            $team1 = $row['team1']."(".$row['team1_short'].")";
                            $team2 = $row['team2']."(".$row['team2_short'].")";
                            $name =  "$team1 vs $team2";
                            $id = $row['id'];
                            echo '<option value="'.$id.'">'.$name.'</option>';
                        }
                
                    } ?>
            </select>
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-primary btn-lg" type="submit">Proceed</button>
    </div>
</form>