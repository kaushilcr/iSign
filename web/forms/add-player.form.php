<?php

?>
<div class="servermessage">
    <?php
echo $this->session->getData("SERVERMESSAGE");$this->session->setData("SERVERMESSAGE",false);
?>
</div>
<form action="<?php echo BASE_URL?>add-player.process" class="form-horizontal" id="register" role="form" method="post" enctype="multipart/form-data">
    <div class="row text-center">
        <div class="col-md-2"></div>
        <div class="col-md-3"><h4>Player Name</h4></div>
        <div class="col-md-3"><h4>Player Type</h4></div>
        <div class="col-md-3"><h4>Photograph <small><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="Allowed files: *.jpg, *.png, *.gif Max size: 100 KB"></span></small></h4>
            
        </div>
    </div>
    <?php
    $i=0;
    while($i!=14)
    {
            $i++;
        ?>
        <div class="form-group required">
            <label for="playername<?php echo $i?>" class="control-label col-md-2">
                Player <?php echo $i;
                    if($i==1) echo ' <abbr title="Captain">(C)</abbr>';
                    if($i==2) echo ' <abbr title="Vice Captain">(VC)</abbr>';
                ?>
            </label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="playername<?php echo $i?>" placeholder="Enter full name here" name="playername[]" required value="<?php if($i==1 && defined('CAPTAINNAME')) echo CAPTAINNAME?>";>
            </div>
            <div class="col-md-3">
                <select name="playertype[]" id="playertype" class="form-control" required>
                    <option value="Batsman">Batsman</option>
                    <option value="Bowler">Bowler</option>
                    <option value="AllRounder">AllRounder</option>
                    <option value="WicketKeeper">WicketKeeper</option>
                </select>
            </div>
            <div class="col-md-3">
                <input type="file" class="" name="playerpic[]" required>
                
            </div>
        </div>
        <?php
    }
    ?>
        <div class="form-group required">

        <div class="col-md-7 col-md-offset-5">

            <button class="btn btn-sm btn-success" type="submit">Submit</button>
            <button class="btn btn-sm btn-default" type="reset">Reset</button>
        </div>
    </div>
</form>
<div class="servermessage"></div>
<?php
    if(!defined('DIGIT_ONLY'))define('DIGIT_ONLY',true);
    if(!defined('AJAXSUBMIT'))define('AJAXSUBMIT',true);
?>