<div class="servermessage"></div>
<form action="<?php echo BASE_URL?>start-match.process" class="form-horizontal" id="register" role="form" method="post" enctype="multipart/form-data">
    <div class="form-group required">
        <label for="match_id_text" class="control-label col-sm-3">
            Scheduled match
        </label>
        <div class="col-sm-7">
            <select name="match_id" id="" class="form-control" id="match_id_text">
               <option value="">Please select</option>
               <?php
                    $sql = "SELECT a.`id`,b.`name` as 'team1' ,b.`short_name` as 'team1_short',c.`name` as 'team2',c.`short_name` as 'team2_short' FROM `match` a, `team` b, `team` c where a.`team1_id` = b.`id` and a.`team2_id` = c.`id` and `started`<>1";
                    $result = $this->db->query($sql);
                    if(!$result)
                        echo '<option value="">Error</option>';
                    else{
                        while($row = mysqli_fetch_array($result))
                        {
                            $team1 = $row['team1']."(".$row['team1_short'].")";
                            $team2 = $row['team2']."(".$row['team2_short'].")";
                            $name =  "$team1 vs $team2";
                            $id = $row['id'];
                            echo '<option value="'.$id.'">'.$name.'</option>';
                        }
                
                    } ?>
            </select>
        </div>
    </div>
    <div class="text-center">
        <span class="h1">Or</span>
    </div>
    <br>
    <div class="form-group required">
        <label for="teamone" class="control-label col-sm-3">
            Team One
        </label>
        <div class="col-sm-7">
            <select name="teamone" id="teamone" class="form-control">
                <option value="">Please select</option>
                <?php
                    $result = $this->db->get("team",array("id","name","short_name"),"freezed","1");
                    while($row = mysqli_fetch_array($result))
                    {
                        $id = $row['id'];
                        $name = $row['name'];

                        $name.="-".$row['short_name'];
                        echo '<option value="'. $id.'">'.$name.'</option>';
                    }
                    
                ?>
            </select>
        </div>
    </div>
    <div class="form-group required">
        <label for="teamtwo" class="control-label col-sm-3">
            Team Two
        </label>
        <div class="col-sm-7">
            <select name="teamtwo" id="teamtwo" class="form-control" >
                <option value="">Please select</option>
                <?php
                    mysqli_data_seek($result, 0);
                    while($row = mysqli_fetch_array($result))
                    {
                        $id = $row['id'];
                        $name = $row['name']."-".$row['short_name'];
                        echo '<option value="'. $id.'">'.$name.'</option>';
                    }
                    
                ?>
                
            </select>
        </div>
    </div>
    <hr>
    <div class="form-group required">
        <label for="tosswon" class="control-label col-sm-3">
            Toss won by
        </label>
        <div class="col-sm-7">
            <select name="tosswon" id="tosswon" class="form-control">
                <option value="teamone">Team One</option>
                <option value="teamtwo">Team Two</option>
            </select>
        </div>
    </div>
    <div class="form-group required">
        <label for="electedto" class="control-label col-sm-3">
            Elected to
        </label>
        <div class="col-sm-7">
            <select name="electedto" id="electedto" class="form-control" required>
               <option value="Bat">Bat</option>
               <option value="Bowl">Ball</option>
            </select>
        </div>
    </div>
    <div class="form-group required">
        <label for="over" class="control-label col-sm-3">
            Over limit
        </label>
        <div class="col-sm-7">
            <input type="number" class="form-control" id="over" min="1" max="50" required name="over">
        </div>
    </div>
    <div class="form-group required">
        <div class="col-sm-7 col-sm-offset-5">
            <button class="btn btn-success btn-lg" type="submit">Start Match</button>
        </div>
    </div>
    
</form>
<?php
    if(!defined('AJAXSUBMIT'))define('AJAXSUBMIT',true);
?>