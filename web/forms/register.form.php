<div class="servermessage"></div>
<form action="<?php echo BASE_URL?>register.process" class="form-horizontal" id="register" role="form" method="post" enctype="multipart/form-data">
    <div class="form-group required">
        <label for="teamname" class="control-label col-sm-3">
            Team Name
            <small><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="Team name to be used. Once fixed, it won't be possible to change it."></span></small>
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control  input-sm" id="teamname" placeholder="Enter your team name here" name="teamname" required>
            <small><p class="help-block">Please ensure proper <b>capitalization</b> of the name.</p></small>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="shortname" class="control-label col-sm-3">
            Short Name
            <small><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="A 4 Alpha Numeric unique short name to represent your team. Once fixed, it won't be possible to change it."></span></small>
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control input-sm livecheck" id="shortname" placeholder="" name="shortname" style="text-transform:uppercase" required>
            <div id="livecheck-result"></div>
            <small><p class="help-block">For Ex. <b><i>IS11</i></b> for <b><i>iSign Eleven</i></b> (ALL CAPITAL)</p></small>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="email" class="control-label col-sm-3">
            Email
        </label>
        <div class="col-sm-9">
            <input type="email" class="form-control input-sm" id="email" placeholder="" name="email" required>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="mobile" class="control-label col-sm-3">
            Mobile
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control input-sm digit-only" id="email" placeholder="10 Digit Mobile number" name="mobile" maxlength="10" required>
            <small><p class="help-block">Do not prefix <i><b>+91</b></i> or <i><b>0</b></i></p></small>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="captainname" class="control-label col-sm-3">
            Captain Name
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control input-sm" id="captainname" placeholder="Full name of caption" name="captainname" required>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="address" class="control-label col-sm-3">
            Address
        </label>
        <div class="col-sm-9">
            <textarea name="address" id="address" class="form-control input-sm" placeholder="Full Address including pin code"></textarea>
        </div>
    </div>
    <div class="form-group form-group-sm required">
        <label for="address" class="control-label col-sm-3">
            Logo
        </label>
        <div class="col-sm-9">
            <input type="file" id="teamlogo" class="form-control input-sm" required name="logo" accept="image/*">
            <small><p class="help-block"><b><i>*.jpg, *.png, *.gif</i></b>, use image with same height and width. Max Resolution <b><i>250x250</i></b>, Max size : <b><i>100 KB</i></b></small>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="control-label col-sm-3"></label>
        <div class="col-sm-9">
            <p class="form-control-static">After Registration, you'll receive your username and password via mail, you can add all your players after login</p>
        </div>
    </div>

    <div class="form-group form-group-sm required">
        <div class="col-sm-7 col-sm-offset-5">
            <button class="btn btn-sm btn-success" type="submit" id="submit">Submit</button>
            <button class="btn btn-sm btn-default" type="reset">Reset</button>
        </div>
    </div>
</form>
<?php
    if(!defined('DIGIT_ONLY'))define('DIGIT_ONLY',true);
    if(!defined('AJAXSUBMIT'))define('AJAXSUBMIT',true);
    if(!defined('LIVE_CHECK'))define('LIVE_CHECK',true);
?>