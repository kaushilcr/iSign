<?php
require_once 'config/config.php';
require_once CONFIG."db.config.php";
@include_once CONFIG."custom.config.php";
require_once CONFIG."general.config.php";
if(DB_REQUIRED)
    require_once CLASSES."db.class.php";

require_once CLASSES."init.class.php";

$init = new init();

if(isset($_GET['load']))
    define ('ACTION',strtolower($_GET['load']));
else
    define ('ACTION','null');

if(isset($_GET['folder']))
    $folder = $_GET['folder']."/";
else
    $folder = "";
define("DIRECT_MODULE",true);

//Check for Authentication here.
$role_code = $init->session->getData("ROLE_CODE");

$result = $init->db->query("SELECT m.* from `_module` m,`_page` p,`_role` r,`_role_page` rp
    WHERE 
        rp.`role_id` = r.`id` AND
        rp.`page_id` = p.`id` AND
        m.`page_id` = m.`page_id` AND
        r.`role_code` = '$role_code'
");

if(MULTILINGUAL) {
    if(file_exists($init->getLanguagePath().MODULE.ACTION.".php")){
        $init->loadLanguage($init->getLanguagePath().MODULE.ACTION.".php");
    }else{
        if(DEBUG) echo "Language not loaded !";
    }
}

if(!$result){
    echo "Error in excuting script: ".$init->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    echo $init->lang['ACCESS_DENIED_TEXT'];
    return;
}

$init->loadModule($folder.ACTION);

?>