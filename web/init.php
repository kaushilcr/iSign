<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
if(defined('AJAX_CALL') && AJAX_CALL) {
    /* special ajax here */
    if(!$init->loadPage("page")) "Not found";
    return;
}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $init->lang['TITLE']?></title>
        <?php $init->loadCSS("bootstrap-datepicker3.standalone.min");?>

        <?php $init->loadCSS("bootstrap_yeti.min");?>
        <?php $init->loadCSS("font-awesome.min");?>
        <?php $init->loadCSS("bootstrap-switch.min");?>
        <?php $init->loadCSS("jquery.dataTables");?>
        <?php $init->loadCSS("dataTables.responsive");?>
        <?php $init->loadCSS("jquery.calculator");?>
        <?php $init->loadCSS("sidebar-menu");?>
        <?php $init->loadCSS("jquery.ipopup");?>
        <?php $init->loadCSS("selectize.bootstrap3");?>
        <?php $init->loadCSS("main");?>
        <?php $init->loadCSS("bootstrap-datetimepicker.min");?>
        <?php $init->loadCSS("nprogress");?>

        <?php $init->loadJS("jquery-1.11.1.min");?>
        <?php $init->loadJS("bootstrap.min");?> 
    </head>
    <body>
        <?php if(!$init->loadPage("page")) "Not found" ;?>
        <?php /*<!-- Mandatory JS-->*/ ?>
        <?php $init->loadJS("jquery-ui.min");?>
        <?php $init->loadJS("notify.min");?>
        <?php $init->loadJS("jquery.form.min");?>
        <?php $init->loadJS("load-module");?>
        <?php $init->loadJS("ajaxsubmit");?>
        <?php $init->loadJS("bootstrap-switch.min");?>
        <?php $init->loadJS("show-password");?>
        <?php $init->loadJS("jquery.print.min");?>
        <?php $init->loadJS("bootstrap-datepicker.min");?>
        <?php $init->loadJS("jquery.dataTables");?>
        <script src="//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js"></script>
        <?php $init->loadJS("jquery.ipopup");?>
        <?php $init->loadJS("selectize.min");?>
        <?php $init->loadJS("bootstrap-datetimepicker.min");?>



        <?php $init->loadJS("nprogress");?>
        <?php $init->loadJS("script");?>
        <?php
/*
        <script>
            var now = new Date(<?php echo time() * 1000 ?>);
            function startInterval(){
                setInterval('updateTime();', 1000);  
            }
            startInterval();//start it right away
            function updateTime(){
                var nowMS = now.getTime();
                nowMS += 1000;
                now.setTime(nowMS);
                var clock = document.getElementById('qwe');
                if(clock){
                    clock.innerHTML = now.toTimeString();//adjust to suit
                }
            }
        </script>
        */
        ?>
    </body>
</html>