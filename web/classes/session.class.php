<?php
 /**
     * show
     * sends image to browser and destroy the resource if headers not sent.
     * use php constants IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG
     *
     * @final
     * @static
     * @access public
     * @param resource $resource
     * @param int $type
     */
class session{
	
	function __construct()
	{
		if(!headers_sent($filename, $linenum))
		{
			session_start();
			if(!isset($_SESSION['__USER_LANGUAGE__'])) $_SESSION['__USER_LANGUAGE__'] = 'en';
		}
		else
			if(defined('DEBUG') && DEBUG) echo "Headers already sent in $filename on line $linenum";
	}
	function setData($data,$value)
	{
		$_SESSION[$data] = $value;
	}
	function getData($data)
	{
        if(isset($_SESSION[$data]))
		      return $_SESSION[$data];
        return false;
	}
	function unsetData($data)
	{
		if(isset($_SESSION[$data]))
		{
			unset($_SESSION[$data]);
			return true;
		}
		return false;
		
	}
	function isLoggedIn()
	{
		return @$_SESSION['__USER_LOGGED_IN__'];
	}
	function login($type="NORMAL")
	{
		//echo "Loged";
		$_SESSION['__USER_LOGGED_IN__'] = true;
		$_SESSION['__USER_TYPE__'] = strtoupper ($type);
		
	}
	function logout()
	{
		session_destroy();

	}
	function getUserType()
	{
		return $_SESSION['__USER_TYPE__'];
	}
	function isAdmin()
	{
		if(isset($_SESSION['__USER_TYPE__']) && $_SESSION['__USER_TYPE__'] =='ADMIN')
			return true;
		return false;
	}
	function toggleLanguage()
	{
		
		if($_SESSION['__USER_LANGUAGE__']=='en')
			$_SESSION['__USER_LANGUAGE__']='gu';
		else
			$_SESSION['__USER_LANGUAGE__']='en';
		return $_SESSION['__USER_LANGUAGE__'];
	}
	function getLanguage()
	{
		return $_SESSION['__USER_LANGUAGE__'];
	}
	function setLanguage($l)
	{
		$_SESSION['__USER_LANGUAGE__']=strtolower($l);
	}
}



?>