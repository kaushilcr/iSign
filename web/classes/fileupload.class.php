<?php
include_once 'db.class.php';
include_once 'json.class.php';
class fileupload{
	private $db;
	private $error,$isMultiple;
	private $JSONObj;

	private $message;
	private $valid_formats ;
    private $max_file_size; //100 kb
	private $path; // Upload directory
	private $count;
	private $file_ary;
	
	private $failedCount;
	public $filenames;
	function __construct($db)
	{
		$this->db=$db;
		$this->JSONObj = new JSON();
		$filenames = array();
		$this->isMultiple = true;
		$this->files = array();
		$this->message = array();
		$this->valid_formats = array("jpg", "jpeg", "png", "gif", "bmp");
		$this->max_file_size = 2000000; 
		$this->path = "img/projects/"; // Upload directory
		$this->count = 0;
		$this->failedCount = 0;
	}

	function setMultiple($bool)
	{
		$this->isMultiple = $bool;
	}
	function uploadMultiple()
	{
		foreach ($_FILES['files']['name'] as $f => $name) 
		{
			if ($_FILES['files']['error'][$f] == 4) {
				$this->failedCount++;
				continue; // Skip file if any error found
			}	       
			if ($_FILES['files']['error'][$f] == 0) {	           
				if ($_FILES['files']['size'][$f] > $this->max_file_size) {
					$this->message[] = "$name is too large!.";
					$this->failedCount++;
					continue; // Skip large files
				}
				elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $this->valid_formats) ){
					$this->message[] = "$name is not a valid format";
					$this->failedCount++;
					continue; // Skip invalid file formats
				}
				else{ // No error found! Move uploaded files 
					$ext = pathinfo($_FILES['files']['name'][$f], PATHINFO_EXTENSION);
					$uniq_name = uniqid() . '.' .$ext;
					$this->filenames[]=$uniq_name;
					//move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path . $uniq_name);
					if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $this->path.$uniq_name)) {
						$this->count++; // Number of successfully uploaded files
					}
				}
			}
		}
	}
	function prepareJSON()
	{
		$msg="";
		foreach ($this->message as $ms) {
			$msg.= $ms;
		}
		
		$this->JSONObj->add("success",($this->count>0)?true:false);
		$this->JSONObj->add("error",$msg);
		$this->JSONObj->add("successcount",$this->count);
		$this->JSONObj->add("failcount",$this->failedCount);
		if($this->failedCount>0 && $this->count>0)
			$this->JSONObj->add("message","Few images uploaded !");
		else if ($this->failedCount==0 && $this->count!=0)
			$this->JSONObj->add("message","All images uploaded !");
		else
			$this->JSONObj->add("message","No Images uploaded");
		return $this->JSONObj->getJSON();
	}
	function uploadSingle()
	{

	}
	function reArrayFiles() {

		$this->file_ary = array();
		$file_count = count($this->files['name']);
		$file_keys = array_keys($this->files);

		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key){
				$this->file_ary[$i][$key] = $this->files[$key][$i];
			}
		}
		return $this->file_ary;
	}
	function upload()
	{
		if($this->isMultiple)
			$this->uploadMultiple();
		else
			$this->uploadSingle();
	}

}
?>