<?php if ( ! defined('BASE_URL')) exit('No direct script access allowed');

class sms{

	/***************************************
	SMS Api v1.0
	Developed by Kaushil Rakhasiya
	(c) iSignIndia.com
	Change log
	
	v1.0	Initial Release															Kaushil Rakhasiya
	v1.1	2-Oct-2014 Edited $to Variable to accept non array value 				Kaushil Rakhasiya
	v1.2	11-Oct-2014 Added Method to send different SMS to different Numbers 	Kaushil Rakhasiya
	
	*****************************************/
	private $error,$ak,$url,$to,$from,$balance,$xml,$xmlPrepared;
	function __construct() {

		$to=array();
		$this->xml = new SimpleXMLElement('<MESSAGE/>');
		$this->from = "WEBSMS";
		$this->url = "http://sms.isignindia.com/api/";
		$this->xmlPrepared=false;
	}
	function setApiURL($url)
	{
		if(substr($url, -1)!='/');
			$this->url=$url."/";
		$this->url=$url;
	}
	function setAuthKey($ak)
	{
		$this->ak=$ak;
	}
	function setAuthenticationKey($ak)  //alias for setAuthKey
	{
		$this->setAuthKey($ak);
	}

	function setTo($to)
	{
		$this->to = $to;
	}
	function setFrom($from)
	{
		$this->from = $from;
	}

	function sendMultiple($data)
	{
		
		if(!is_array($data))
		{
			$this->error = "Not an array(1)";
			return false;
		}
		$authkey = $this->xml->addChild('AUTHKEY',$this->ak);
		$sender = $this->xml->addChild('SENDER',$this->from);
		foreach($data as $a)
		{
			if(!is_array($a))
			{
				$this->error = "Not an array(1)";
				return false;
			}
			$sms = $this->xml->addChild('SMS');
			if(!is_array($a['to']))
			{
				$address = $sms->addChild('ADDRESS','');
				$address->addAttribute('TO', $a['to']);
			}
			else
			{
				foreach ($a['to'] as $to){
					$address = $sms->addChild('ADDRESS','');
					$address->addAttribute('TO', $to);
				}
			}
			$sms->addAttribute('TEXT', $a['msg']);
		}
		$data =  $this->xml->asXML();
		//echo $data;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$this->url."postsms.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$data);
		$result = curl_exec ($ch);
		curl_close ($ch);
		if(strlen($result)==24)
			return $result;
		$this->error = "Error in sending SMS(".$result.")";
		return false;
	}
	function setMessage($msg)
	{
		$this->msg= preg_replace('/\s+/', ' ', trim($msg));
	}
	function getBalance()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$this->url."balance.php?authkey=".$this->ak."&type=info");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$this->balance = curl_exec ($ch);
		curl_close ($ch); 
		return $this->balance;
	}
	function prepareXML()
	{
		$authkey = $this->xml->addChild('AUTHKEY',$this->ak);
		$sender = $this->xml->addChild('SENDER',$this->from);
		$sms = $this->xml->addChild('SMS');
		if(is_array($this->to))
		{
			foreach ($this->to as $to){
				$address = $sms->addChild('ADDRESS','');
				$address->addAttribute('TO', $to);
			}
		}else
		{
			$address = $sms->addChild('ADDRESS','');
			$address->addAttribute('TO', $this->to);
		}
		$sms->addAttribute('TEXT', $this->msg);
		$this->xmlPrepared=true;
	}
	function getXML()
	{
		if(!$this->xmlPrepared)
			$this->prepareXML();
		return $this->xml->asXML();
	}
	function getError()
	{
		return $this->error;
	}
	function sendSMS($prepareXML=false)
	{
		if(!($this->xmlPrepared) && $prepareXML)
		{
			$this->prepareXML();
		}
		if($this->xmlPrepared)
		{
			$data = $this->xml->asXML();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$this->url."postsms.php");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$data);
			$result = curl_exec ($ch);
			curl_close ($ch);
			if(strlen($result)==24)
				return $result;
			$this->error = "Error in sending SMS(".$result.")";
			return false;
		}
		else{
			$this->error = "XML not prepared!";
			return false;
		}
	}

}
?>