<?php
	/************************************
		(c) iSignIndia.com
		db.class.php
		Change Log
		30 Aug 2014  - v1.1 		Kaushil Rakhasiya 				Added two Function get and put
	************************************/
class DB{
	private $con;
	public $affectedRow;
	function __construct() {
		$this->con = @mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_DATABASE);
		//$mysqli->set_charset("utf8");
        if (!$this->con) {
            die('Connect Error: ' . mysqli_connect_error());
        }
		mysqli_set_charset($this->con,"utf8");
		mysqli_query($this->con ,"SET character_set_results=utf8");
	}
	public function query($sql){
		$result = mysqli_query($this->con,$sql);
		$this->affectedRow = mysqli_affected_rows($this->con);
		return $result;
	}
	public function error(){
		return mysqli_error($this->con);
	}
    public function errorCode(){
        return mysqli_errno($this->con);
    }
	public function insertId(){
		return mysqli_insert_id($this->con);
	}
	public function beginTransaction(){
		mysqli_query($this->con,"begin");
	}
	public function rollbackTransaction()
	{
		mysqli_query($this->con,"rollback");
	}
	public function commitTransaction()
	{
		mysqli_query($this->con,"commit");
	}
	public function escape($value)
	{
		return mysqli_real_escape_string($this->con,$value);
	}
	public function get($table,$cols="*",$pk=false,$pkv=false)
	{
		$columns="";
		if(is_array($cols))
		{
			foreach($cols as $c)
			{
				$columns .= "`".$c."`,";
			}
			$columns = rtrim($columns, ',');
		}
		else if($cols!="*")
			$columns = "`".$cols."`";
		else
			$columns = $cols;
		$columns = strtolower($columns);
		$where="";
		if($pk && $pkv)
		{
			$where = " WHERE `".$pk."`='".$pkv."'";
		}
		$sql="select ".$columns." from `$table`".$where;
		//echo $sql;
		return $this->query($sql);
		
	}
	public function getWhere($table,$cols="*",$key=false,$val=false,$AND=true)
	{
		$columns="";
		$tables="";
		if(is_array($table))
		{
			foreach($table as $t)
				$tables .= "`".$t."`,";
			$tables = rtrim($tables, ',');
		}
		else
			$tables=$table;
		if(is_array($cols))
		{
			foreach($cols as $c)
			{
				$columns .= "`".$c."`,";
			}
			$columns = rtrim($columns, ',');
		}
		else if($cols!="*")
			$columns = "`".$cols."`";
		else
			$columns = $cols;
		$columns = strtolower($columns);
		$where="";
		$whereAry=array();
		if(is_array($key) && is_array($val))
		{
			$i=0;
			while(@$key[$i])
			{
				/*if (strpos($val[$i],' ') !== false) {
					$val[$i] = "'".$val[$i]."'";
				}*/
				$whereAry[]="`".$key[$i]."`=". $val[$i]." ";
				$i++;
			}
			if($AND)
				$where = implode (" AND ",$whereAry);
			else
				$where = implode (" OR ",$whereAry);
			$where = " WHERE ".$where;
		}
		else if($key && $val)
		{
			$where = " WHERE `".$key."`='".$val."'";
		}
		$sql="select ".$columns." from ".$tables." ".$where;
		//echo $sql;
		return $this->query($sql);
	}
	public function getCount($table,$cols="*",$pk=false,$pkv=false)
	{
		if($cols!="*")
			$cols = "`".$cols."`";
		$columns = "count(".$cols.") as 'count'";
		$columns = strtolower($columns);
		$where="";
		if($pk && $pkv)
		{
			$where = " WHERE `".$pk."`='".$pkv."'";
		}
		$sql="select ".$columns." from `$table`".$where;
		//if (DEBUG) echo $sql;
		$result = $this->query($sql);
		if(!$result){
			if(DEBUG)
				echo $this->error();
			return 0;
		}
		$row  = mysqli_fetch_array($result);
		return $row['count'];
	}
    public function getMax($table,$cols="*",$pk=false,$pkv=false)
	{
		if($cols!="*")
			$cols = "`".$cols."`";
		$columns = "max(".$cols.") as 'max'";
		$columns = strtolower($columns);
		$where="";
		if($pk && $pkv)
		{
			$where = " WHERE `".$pk."`='".$pkv."'";
		}
		$sql="select ".$columns." from `$table`".$where;
		//if (DEBUG) echo $sql;
		$result = $this->query($sql);
		if(!$result){
			if(DEBUG)
				echo $this->error();
			return 0;
		}
		$row  = mysqli_fetch_array($result);
		return $row['max'];
	}
	public function getDistinct($table,$cols="*",$pk=false,$pkv=false)
	{
		if($cols!="*")
			$cols = "`".$cols."`";
		$columns = "DISTINCT ".$cols;
		$where="";
		if($pk && $pkv)
		{
			$where = " WHERE `".$pk."`='".$pkv."'";
		}
		$sql="select ".$columns." from `$table`".$where. " order by $cols";
		if (DEBUG) echo $sql;
		$result = $this->query($sql);
		if(!$result){
			if(DEBUG)
				echo $this->error();
			return false;
		}
		//$row  = mysqli_fetch_array($result);
		return $result;
	}
	public function put($table,$cols)
	{
		$columns="";
		$values="";
		if(is_array($cols))
		{
			$keys=array_keys($cols);
			foreach($keys as $c)
			{
				$columns .= "`".$c."`,";
			}
			$columns = trim($columns, ",");
			strtolower($columns);
			foreach($cols as $c)
			{
				$c=$this->escape($c);
				$values .= "'".$c."',";
			}
			$values = trim($values, ",");
			strtolower($values);
		}
		else
			return false;
		//$values = $this->escape($values);
		$sql = "insert into `$table` (".$columns.") values(".$values.")";
		//echo $sql;
		$result  = $this->query($sql);
        if($result)
        {
            return $this->insertId();
        }
        else
            return $result;
	}
    public function replace($table,$cols)
	{
		$columns="";
		$values="";
		if(is_array($cols))
		{
			$keys=array_keys($cols);
			foreach($keys as $c)
			{
				$columns .= "`".$c."`,";
			}
			$columns = trim($columns, ",");
			strtolower($columns);
			foreach($cols as $c)
			{
				$c=$this->escape($c);
				$values .= "'".$c."',";
			}
			$values = trim($values, ",");
			strtolower($values);
		}
		else
			return false;
		//$values = $this->escape($values);
		$sql = "replace into `$table` (".$columns.") values(".$values.")";
		//echo $sql;
		$result  = $this->query($sql);
        if($result)
        {
            return $this->insertId();
        }
        else
            return $result;
	}
	public function update($table,$cols,$whereAry,$AND = true)
	{
		$columns = "";
		if(is_array($cols))
		{
			foreach($cols as $k => $v)
			{
				$columns .= "`".$k. "`='" .$v."',";
			}
			$columns = trim($columns, ",");
		}
		else
			return false;
		$where =" where ";
		if(is_array($whereAry))
		{
			foreach($whereAry as $k => $v)
			{
				if($AND)
					$where.= $k ."='".$v."' AND ";
				else	
					$where.= $k ."='".$v."' OR ";
			}
			$where = trim($where, "AND ");
			$where = trim($where, "OR ");
		}
		else
			return false;
		$sql="update `$table` set $columns $where";
		//echo $sql;
		return $this->query($sql);
	}
	public function delete($table,$pk,$pkv)
	{
		$sql = "delete from `$table` where `$pk` = '$pkv'";
		//echo $sql;
		return $this->query($sql);
	}

}
?>