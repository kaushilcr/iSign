<?php
if(SESSION_ENABLED) include_once 'session.class.php';
class init{
    public $db,$session,$lang;
    private $data,$lang_selected,$lang_path;
    function __construct()
    {
        if(DB_REQUIRED)
            $this->db = new db();
        else
            $this->db = false;
        if(SESSION_ENABLED)
            $this->session = new session();
        else
            $this->session = false;
        if(MULTILINGUAL){
            $this->lang = array();
            $this->lang_selected = $this->session->getLanguage();
            $this->lang_path = LANGUAGE.$this->lang_selected."/";
            include $this->lang_path."common.php";
        }
        else
            $this->lang = false;
        $this->data = array();
    }
    function loadPage($page)
    {
        //echo PAGES.$page.".php";
        $path=PAGES;
        if(MULTILINGUAL) {
            if(file_exists($this->lang_path.PAGES.$page.".php")){
                include $this->lang_path.PAGES.$page.".php";
            }else{
                if(DEBUG) echo "Language not loaded !";
                include $this->lang_path.PAGES."404".".php";
            }
        }
        if(file_exists($path.$page.".php")){
            include $path.$page.".php";
            return true;
        }
        else{
            if(DEBUG) echo $path.$page.".php";
            include PAGES."404".".php";
            return false;
        }
    }
    function loadModule($module,&$data=0)
    {
        if(MULTILINGUAL) {
            if(file_exists($this->lang_path.MODULE.$module.".php")){
                $this->loadLanguage($this->lang_path.MODULE.$module.".php");
            }else{
                if(DEBUG) echo "Language not loaded !";
            }
        }
        if(file_exists(MODULE.$module.".module.php")){
            include MODULE.$module.".module.php";
            return true;
        }
        else{
            $error = "Module not found !";
            return false;
        }
    }
    function loadClass($class,&$data=false)
    {
        if(file_exists(CLASSES.$class.".class.php")){
            include_once CLASSES.$class.".class.php";
            if($data)
                return new $class($data);
            return new $class();
        }	
        else
            return false;
    }
    function loadProcess($process)
    {
        if(MULTILINGUAL) {
            if(file_exists($this->lang_path.PROCESS.$process.".php")){
                include_once $this->lang_path.PROCESS.$process.".php";
            }else{
                if(DEBUG) echo "Language not loaded !";
            }
        }
        if(file_exists(PROCESS.$process.".process.php")){
            include_once PROCESS.$process.".process.php";
            return true;
        }	
        else
        {
            include_once PROCESS."null".".process.php";
            return false;
        }
    }
    function loadAPI($api,$version="v1")
    {
        if(file_exists(API.$version."/".$api.".api.php")){
            include_once API.$version."/".$api.".api.php";
            return true;
        }	
        else if(file_exists(API.$version."/null.api.php"))
        {
            include_once API.$version."/"."null.api.php";
            return false;
        }
        else{
            include_once API."null.api.php";
            return false;
        }
    }
    function loadFunctions($fn)
    {
        if(file_exists(FUNCTIONS.$fn.".functions.php")){
            include_once FUNCTIONS.$fn.".functions.php";
            return true;
        }	
        else
        {
            return false;
        }
    }
    function loadView($location,$view)
    {
        if(file_exists(PAGES.$location."/".$view.".view.php")){
            include_once PAGES.$location."/".$view.".view.php";
            return true;
        }	
        else
        {
            echo "Not found !" ;
            if(DEBUG) echo PAGES.$location."/".$view.".view.php";
            return false;
        }
    }
    function loadJS($js)
    {
        if(file_exists(JS.$js.".js")){
            echo '<script src="'.BASE_URL.JS.$js.'.js"> </script>';
            //include_once BASE_URL.JS.$js.".js";
            return true;
        }
        else
        {
            //echo "Not found !" ;
            if(DEBUG) echo BASE_URL.JS.$js.".js";
            return false;
        }
    }
    function loadCSS($css)
    {
        if(file_exists(CSS.$css.".css")){
            echo '<link href="'.BASE_URL.CSS.$css.'.css" rel="stylesheet">';
            return true;
        }	
        else
        {
            //echo "Not found !" ;
            if(DEBUG) echo BASE_URL.CSS.$css.".css";
            return false;
        }
    }
    function loadForm($form)
    {
        $formext = ".form.php";
        if(file_exists(FORMS.$form.$formext))
        {
            include (FORMS.$form.$formext);
            return true;
        }
        // Form is not generated, hence generate first, save it, and then include.
        $formObj = $this->loadClass("form");
        $formObj->generateForm($form);
        include (FORMS.$form.$formext);
        return true;
    }
    function authenticateFirst($type="NORMAL")
    {
        if($this->session->isLoggedIn())
        {
            if(strtoupper($type) == $this->session->getUserType() )
                return true;
            if(!defined ("AUTHMESSAGE"))define("AUTHMESSAGE","Please Login with authorized Credentials");
        }
        if(!defined ("AUTHMESSAGE")) define("AUTHMESSAGE","Please Login with authorized Credentials");

        $this->loadPage('login');
        return false;
    }
    function isAuthorized($type="NORMAL")
    {
        if($this->session->isLoggedIn())
        {
            if($type =="ADMIN" && $this->session->getUserType()==$type)
                return true;
            else if($type=="NORMAL" && $this->session->getUserType()==$type)
                return true;
        }
        return false;
    }
    function generatePassword($length = 8,$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {

        $count = mb_strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }
    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    function isFieldValid(&$fieldName)
    {
        if(!is_array($fieldName))
            return isset($_REQUEST["$fieldName"]);
        $missing = array();
        foreach($fieldName as $name)
        {
            if(!isset($_REQUEST["$name"]))
                $missing[]=$name;
        }
        $fieldName = $missing;
        if(empty($missing))
            return true;
        else
            return false;
    }
    function additionalData($key,$val)
    {
        $this->data[$key]=$val;
    }
    function addData($key,$val)
    {
        $this->additionalData($key,$val);
    }
    function getData($key)
    {
        return @$this->data[$key];
    }
    function getLanguagePath(){
        return $this->lang_path;
    }
    function loadLanguage($path){
        include_once $path;
    }
}
?>
