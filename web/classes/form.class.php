<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');

/* form Builder class which generates HTML Code for different Form Elements 
    Bootstrap classes will be used for CSS
*/

?>
<?php
class form
{
    private function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
    private function generateTag($name,$attribute){

        $html = '<'.$name;
        if($name=="option")
        {
            return (implode("\r\n",$attribute));
        }

        foreach ($attribute as $key=>$attr){
            $attr = trim($attr);
            $html.=" $key=\"$attr\"";
        }
        $html.=">";
        if($name=="textarea")
            $html.="</textarea>";
        return $html;
    }
    function generateForm($form)
    {
        $formext = ".form.php";
        if(file_exists(FORMS.$form.$formext))
        {
            include (FORMS.$form.$formext);
            return true;
        }
        // Form is not generated, hence generate first, save it, and then include.
        $rawext = ".raw.php";   
        $rawpath = "raw/";
        $handle = fopen(FORMS.$rawpath.$form.$rawext, "r");
        $formFile = fopen(FORMS.$form.$formext, "w");
        $element  = array();
        if ($handle) {
            $previouTag ="";
            $formTags = array();
            while (($line = fgets($handle)) !== false) {
                // process the line read.

                $attributes = explode ("~",$line);
                $formTags = $this->multiexplode(array(".","#","$"),$attributes[0]);

                if($previouTag=="option" && $formTags[0]!="option"){
                    $last_element = array_pop($element);
                    $last_element['html'] = $last_element['html']."</select>";
                    $element[] = $last_element;
                    //fwrite($formFile,"</select>");
                }
                $dotPos = strpos($attributes[0],".");
                $hashPos = strpos($attributes[0],"#");
                $dallarPos = strpos($attributes[0],"$");
                $attr = array();
                if($dotPos!==false && $hashPos!==false)
                {
                    if($dotPos>$hashPos)
                    {
                        $attr['id']=$formTags[1];
                        $attr['class']=$formTags[2];
                    }
                    else{
                        $attr['id']=$formTags[2];
                        $attr['class']=$formTags[1];
                    }
                }
                else if($dotPos!==false){
                    $attr['class']=$formTags[1];
                }
                else if($hashPos!==false){
                    $attr['id']=$formTags[1];
                }
                
                if($dallarPos!==false)
                {
                     $label=end($formTags);
                }


                array_shift($attributes);

                if($formTags[0]=="form")
                    $attr['class'] = @$attr['class']." form-horizontal";
                else if($formTags[0]=="input" or $formTags[0]=="select" or $formTags[0]=="textarea")
                    $attr['class'] = @$attr['class']." form-control";
                    $options=array();
                foreach($attributes as $attrList)
                {
                    $a = explode ("%",$attrList);
                    if($formTags[0]=="option")
                    {
                        $options[]="<option value=\"$a[0]\">$a[1]</option>\r\n";
                        continue;
                    }
                    if($a[0]=="action")
                        $a[1]= BASE_URL.$a[1];
                    $attr[$a[0]] = $a[1];

                }
                if($formTags[0]=="option"){
                    //fwrite($formFile,$this->generateTag($formTags[0],$options));
                    $last_element = array_pop($element);
                    $last_element['html'] = $last_element['html'].$this->generateTag($formTags[0],$options);
                    $element[] = $last_element;
                }
                elseif($formTags[0]=="form")
                {
                    $formTagStart = $this->generateTag($formTags[0],$attr);
                }
                else{
                    //fwrite($formFile,$this->generateTag($formTags[0],$attr));
                    $element[] = array(
                        'label'=> $label,
                        'html' => $this->generateTag($formTags[0],$attr),
                    );
                    $label="";
                }
                fwrite($formFile,"\r\n");
                $previouTag=$formTags[0];
            }
            if($previouTag =="option")
            {
                $last_element = array_pop($element);
                $last_element['html'] = $last_element['html']."</select>";
                $element[] = $last_element;

                //fwrite($formFile,"</select>");
            }
        } else {
            // error opening the file.
        }
        fclose($handle);
        $template = '<div class="form-group">
                        <label class="col-sm-2 control-label">__LABEL__</label>
                        <div class="col-sm-10">
                            __ELEMENT__
                        </div>
                    </div>';
        fwrite($formFile,$formTagStart);
        foreach($element as $ele)
        {
            $template2 = str_replace("__ELEMENT__",$ele['html'],$template);
            $template2 = str_replace("__LABEL__",$ele['label'],$template2);
            fwrite($formFile,$template2);
        }
        fwrite($formFile,"</form>");
        fclose($formFile);
    }

}
?>
