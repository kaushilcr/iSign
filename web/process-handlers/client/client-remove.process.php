<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php

$json = $this->loadClass("json");

if(!isset($_REQUEST['customer_id'])) $json->sendJSON("Customer id is not set");
$customer_id=$_REQUEST['customer_id'];

$result = $this->db->delete("customer","id",$customer_id);
if(!$result){
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove module, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode());
}

$json->add("reloadModule","#customer");
$json->sendJSON("Job Removed successfully",true);
?>