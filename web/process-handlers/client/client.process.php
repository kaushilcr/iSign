<?php if(! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
if(DEBUG) $json->add("debug",var_export($_REQUEST,true));
if(isset($_REQUEST['process-type']) && $_REQUEST['process-type']=="list"){
    $this->loadProcess("client/client-list");
    return;
}
if(!isset($_REQUEST['customer_id']) or empty($_REQUEST['customer_id']))  $json->sendJSON("customer_id is not selected.");
$customer_id = $_REQUEST['customer_id'];

if(!isset($_REQUEST['customer_name'])) $json->sendJSON("customer_name not set");
if(!isset($_REQUEST['address'])) $json->sendJSON("address not set");
if(!isset($_REQUEST['contact_no'])) $json->sendJSON("contact_no is not set"); 
if(!isset($_REQUEST['profession'])) $json->sendJSON("profession is not set"); 
if(!isset($_REQUEST['company_name'])) $json->sendJSON("company_name is not set"); 
if(!isset($_REQUEST['family_members'])) $json->sendJSON("no of family_member is not set"); 
if(!isset($_REQUEST['annual_income'])) $json->sendJSON("annual_income is not set"); 
if(!isset($_REQUEST['budget'])) $json->sendJSON("budget is not set"); 
if(!isset($_REQUEST['intrested_in'])) $json->sendJSON("intrested_in is not set"); 

$customer_name = trim($_REQUEST['customer_name']);
$address = trim($_REQUEST['address']);
$contact_no = trim($_REQUEST['contact_no']);
$profession = trim($_REQUEST['profession']);
$company_name = trim($_REQUEST['company_name']);
$family_members = trim($_REQUEST['family_members']);
$annual_income = trim($_REQUEST['annual_income']);
$budget = trim($_REQUEST['budget']);
$intrested_in = trim($_REQUEST['intrested_in']);

if($customer_id=="new") $new = true; else $new=false;

$cust = array();
$cust['customer_name']=$customer_name;
$cust['address']=$address;
$cust['contact_no']=$contact_no;
$cust['profession']=$profession;
$cust['company_name']=$company_name;
$cust['family_members']=$family_members;
$cust['annual_income']=$annual_income;
$cust['budget']=$budget;
$cust['intrested_in']=$intrested_in;

$this->db->beginTransaction();
if($new){
    $customer_id = $this->db->put("customer",$cust);
    if(!$customer_id) $json->sendJSON("Failed to save data".$this->db->error());
}else{
    $result = $this->db->update("customer",$cust,array("id"=>$customer_id));
    if(!$result) $json->sendJSON("Failed to save data".$this->db->error());
}
$this->db->commitTransaction();
$json->sendJSON("Data has been saved successfully",true);
?>