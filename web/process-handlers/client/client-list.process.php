<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php

$json=$this->loadClass("json");

$entity = "Client";
$property = array();

//Get List of column names
$sql = "SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
      'MAX(IF(pv.name = ''',
      p.name,
      ''', pv.value, NULL)) AS ',
      p.name
    )
  )
FROM _property p, _entity e
WHERE e.`id`= p.`entity_id` AND e.`name`='$entity'";

$result = $this->db->query($sql);
if(!$result){$json->add("error","Error while Executing query: ".$this->db->error());$json->echoJSON();}

$row = mysqli_fetch_array($result);

$subquery = $row[0];
$sql = "SELECT  ev.id,pv.`entity_name`,
        $subquery
FROM `_entity_value` ev
LEFT JOIN (select pv.id, p.id as `property_id`,pv.`entity_value_id`, p.`name`, pv.`value`, e.`name` as `entity_name`
FROM `_property_value` pv, `_property` p, `_entity` e, `_entity_value` ev
WHERE p.`id` = pv.`property_id` AND
      ev.`entity_id` = e.`id` AND
      pv.`entity_value_id` = ev.`id`
ORDER BY pv.`id`) AS pv ON ev.id = pv.entity_value_id
GROUP BY pv.entity_value_id
HAVING entity_name='$entity' ";

$innerJson=$this->loadClass("json");

$count = $this->db->query("select count(*) as total from `_entity` e, `_entity_value` ev
WHERE ev.`entity_id` = e.`id`
AND e.`name` = '$entity'");
$row = mysqli_fetch_array($count);
$count = (int)$row[0];
$json->add("recordsTotal",$row[0]);

$json->add("draw",@$_REQUEST['draw']+0);

$start = $_REQUEST['start'];
$length = $_REQUEST['length'];
$search = $_REQUEST['search'];

$searchTerm = $search['value'];
$where = ""; 
if(!empty($searchTerm)){
    $where.= "AND (";
    $where.= "`first_name` like '%$searchTerm%' or `last_name` like '%$searchTerm%'";
    $where.=") ";
}

//$result = $this->db->query("select * from customer $where order by 'id' desc LIMIT $start, $length");
$result = $this->db->query("$sql $where LIMIT $start, $length");

if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$totalFiltered = mysqli_num_rows($result);

$json->add("recordsFiltered",(!empty($searchTerm)) ? $totalFiltered : $count);
$i=0;
while($row = mysqli_fetch_array($result)){

    $data = array();
    $data[] = $row['first_name'];
    $data[] = $row['last_name'];
    $data[] = $row['address'];

    $data[] =  '<button class="btn btn-default btn-xxs" onclick="viewClient('.$row['id'].')"><i class="fa fa-eye"></i></button>'."&nbsp;".'<button class="btn btn-default btn-xxs" onclick="editClient('.$row['id'].')"><i class="fa fa-pencil"></i></button>'."&nbsp;".'<form action="'.BASE_URL.'customer-remove.process" method="post" style="display:inline-block;"><input type="hidden" name="customer_id" value="'.$row['id'].'"><button class="btn btn-default btn-xxs" onclick="deleteClient('.$row['id'].')"><i class="fa fa-times"></i></button></form>';

    $innerJson->add($i++,$data);
}
$json->add("data",$innerJson->getArray());
$json->echoJSON();
?>