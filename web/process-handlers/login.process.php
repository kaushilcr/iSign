<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
if(!isset($_POST['username']) or !isset($_POST['password']))
{
    $json->add("success",false);
    $json->add("message","Both username and passwords are required fields.");
    $json->echoJSON();
}
if(empty($_POST['username']) or empty($_POST['password']))
{
    $json->add("success",false);
    $json->add("message","Both username and passwords are required fields.");
    $json->echoJSON();
}
/*if(!isset($_POST['tc']))
{
    $json->add("success",false);
    $json->add("message","You must accept terms and condition !");
    $json->echoJSON();
}*/
$u = strtolower($_POST['username']);
$p =  md5("&_&_&_".$_POST['password']."_@_@_@");
/*if($u=="admin1" and $p=="admin@123")
{
    //$_SESSION['username']="Kaushil";
    //$_SESSION['level']="admin";
    //$_SESSION['fid']=0;
    //var_dump($init);
    $this->session->login("ADMIN");
    $this->session->setData("USERNAME","Admin");
    $json->add("success",true);
    $json->add("redirect",BASE_URL."admin.html");
    //$json->add("reload",true);
    $json->add("message","Admin logged in. Please Reload the page if it doesn't redirect you automatically.");
    $json->echoJSON();
}
else*/
{
    $u = strtoupper($u);
    $sql = "SELECT
            u.`id`,
            u.`role_id`,
            r.`role_code` as role_code,
            r.`role_name` as role_name,
            DATE_FORMAT(u.last_login,'%b %d, %Y %h:%i %p') as last_login,
            u.`first_name` as first_name,
            u.`last_name` as last_name,
            u.`photo`,
            u.`active`
        FROM `_user` u,`_role` r
        WHERE
            u.`role_id` = r.`id` and
            u.`username`='$u' and
            u.`password`='$p'";
    $results = $this->db->query($sql);
    if(!$results)
        $json->sendJSON("Error in Executing Query. :".$this->db->error());
    //return total count
    $isLoggedIn = mysqli_num_rows($results); //total records
    $row = mysqli_fetch_array($results);
    if($isLoggedIn)
    {
        if($row['active']==0){
            $json->sendJSON("User is inactive");
        }
        $this->session->login($row['role_code']);
        $id = $row['id'];
        //Update Last Login Date
        $this->db->query("update `_user` set last_login=NOW() where id=$id");
        $result = $this->db->get("_user","last_login","id",$id);
        $temp_row = mysqli_fetch_array($result);

        $login_history = array();

        $login_history['user_id'] = $id;
        $login_history['in_time'] = $temp_row['last_login'];
        $login_history['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->db->put("login_history",$login_history);


        $this->session->setData("LOGIN_ID",$id);
        $this->session->setData("ROLE",$row['role_name']);
        $this->session->setData("ROLE_CODE",$row['role_code']);
        $this->session->setData("LAST_LOGIN",$row['last_login']);
        $this->session->setData("FIRST_NAME",$row['first_name']);
        $this->session->setData("LAST_NAME",$row['last_name']);
        $this->session->setData("PROFILE_PIC",$row['photo']);

        //$json->add("redirect",BASE_URL."default.html");

        $json->add("reload",true);
        /*if($row['role_code']=="admin")
            $json->add("redirect",BASE_URL."admin-dashboard.html");
        if($row['role_code']=="receptionist")
            $json->add("redirect",BASE_URL."home.html");

        if($row['role_code']=="doctor")
            $json->add("redirect",BASE_URL."doctor.html");

        if($row['role_code']=="travelmanager")
            $json->add("redirect",BASE_URL."travelmanager.html");*/


        $json->sendJSON("User logged In. Please Reload the page if it doesn't redirect you automatically.",true);
    }
    $json->sendJSON("Incorrect Username or password");
}
$json->add("success",false);
$json->add("reload",true);
$json->add("message","Something went wrong ! No Condition matched. !");
?>