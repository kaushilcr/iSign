<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
if(!isset($_REQUEST['entity_id'])) $json->sendJSON("entity_id not set");
$entity_id = $_REQUEST['entity_id'];
if(!ctype_digit($entity_id)) $json->sendJSON("Invalid Entity ID");
$result = $this->db->delete("_entity","id",$entity_id);
$this->db->errorCode();
if($this->db->errorCode() == 1451) $json->sendJSON("_entity is linked.");
if(!$result) $json->sendJSON("Error in executing query: ".$this->db->error());
if($this->db->affectedRow<=0) $json->sendJSON("No Entity deleted",true);
/*if(!defined("FROM_API")){$json->add("reloadModule","#staff");}*/
$json->add("reloadModule","#entity");
$json->sendJSON("Entity deleted.",true);
