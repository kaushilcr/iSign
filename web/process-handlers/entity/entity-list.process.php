<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php

$colums[0] = "`name`";
$colums[1] = "`property_count`";
$colums[2] = "`total`";


$json=$this->loadClass("json");
$innerJson=$this->loadClass("json");

$json->add("draw",@$_REQUEST['draw']+0);

$start = $_REQUEST['start'];
$length = $_REQUEST['length'];
$search = $_REQUEST['search'];
$order = $_REQUEST['order'];
$searchTerm = $search['value'];

$order_by = $colums[$order[0]['column']];
$order_dir = $order[0]['dir'];

$result = $this->db->query("select count(*) as total FROM _entity");

if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$total = mysqli_fetch_array($result);
$json->add("recordsTotal",$total['total']);

$where = "WHERE 1=1 "; 
if(!empty($searchTerm)){
    $where.= "AND (";
    $where.= "`name` like '%$searchTerm%'";
    $where.=") ";
}

$result = $this->db->query("select e.*, (select count(*) from `_entity_value` where `entity_id` = e.`id`) as total,(select count(*) from `_property` where `entity_id` = e.`id`) as property_count  from `_entity` e
 $where order by $order_by $order_dir LIMIT $start, $length");
if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$totalFiltered = mysqli_num_rows($result);

$json->add("recordsFiltered",(!empty($searchTerm)) ? $totalFiltered : $total['total']);
$i=0;
while($row = mysqli_fetch_array($result)){

    $data = array();
    $data[] = $row['name'];
    $data[] = $row['property_count'];
    $data[] = $row['total'];

    $data[] = '<button class="btn btn-default btn-xxs" onclick="editMaterial('.$row['id'].')"><i class="fa fa-pencil"></i></button>'."&nbsp;".'<form action="'.BASE_URL.'entity/entity.process?process-type=remove" method="post" style="display:inline-block;"><input type="hidden" name="entity_id" value="'.$row['id'].'"><button  type="submit" class="btn btn-default btn-xxs" onclick="deletematerial('.$row['id'].')"  data-confirm="Are you sure to delete Material ?"><i class="fa fa-times"></i></button></form>';

    $innerJson->add($i++,$data);
}

$json->add("data",$innerJson->getArray());

$json->echoJSON();

?>