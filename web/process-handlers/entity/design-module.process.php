<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['module_id'])) $json->sendJSON("module not set");
if(!isset($_REQUEST['entity_id'])) $json->sendJSON("entity not set");

$entity_id = $_REQUEST['entity_id'];
$module_id = $_REQUEST['module_id'];


$entity_module = array();

$entity_module['entity_id'] = $entity_id;
$entity_module['module_id'] = $module_id;


$this->db->beginTransaction();

$result = $this->db->replace("_entity_module",$entity_module);
if(!$result) $json->sendJSON("Error while inserting entry.: ".$this->db->error());

$link = "entity/entity.module?entity_module_link=".$result;
$link2 = "entity/entity.process?entity_module_link=".$result;
$result = $this->db->update("_module",array("module_name"=>"$link","process_name"=>"$link2",),array("id"=>$module_id));
if(!$result) $json->sendJSON("Error while updating entry.: ".$this->db->error());


$this->db->commitTransaction();
$json->sendJSON("Module Entity Linked/Updated",true);


?>