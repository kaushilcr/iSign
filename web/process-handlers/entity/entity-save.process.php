<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['entity_id']))   $json->sendJSON("entity_id not set"); 
if(!isset($_REQUEST['entity_name']))   $json->sendJSON("entity_name not set"); 
if(!isset($_REQUEST['property_name']))   $json->sendJSON("property_name not set"); 
if(!isset($_REQUEST['property_type']))   $json->sendJSON("property_type not set"); 
if(!isset($_REQUEST['property_display_name']))   $json->sendJSON("property_display_name not set"); 


$entity_id = $_REQUEST['entity_id'];
$entity_name = $_REQUEST['entity_name'];
$property_name = $_REQUEST['property_name'];
$property_type = $_REQUEST['property_type'];
$property_display_name = $_REQUEST['property_display_name'];

$this->db->beginTransaction();

$entity = array ();

$entity['name'] = $entity_name;

$result = $this->db->put("_entity",$entity);
if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());
$entity_id = $result;

for($i=0;$i<count($property_type);$i++){
    $property = array();
    $property['name'] = $property_name[$i];
    $property['type'] = $property_type[$i];
    $property['display_name'] = $property_display_name[$i];
    $property['entity_id'] = $entity_id;
    $result = $this->db->put("_property",$property);
    if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());
}

$this->db->commitTransaction();

$json->sendJSON("Entity Details saved successfully.",true);

?>