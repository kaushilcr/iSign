<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<?php
$json=$this->loadClass("json");
if(DEBUG) $json->add("debug",var_export($_REQUEST,true));
if(isset($_REQUEST['entity_module_link'])){
    $this->loadProcess("entity/entity/entity");
}
else
{
    if(!isset($_REQUEST['process-type'])){
        $json->sendJSON("process-type not selected.");
    }
    $process_type = $_REQUEST['process-type'];

    if($process_type=="list")    $this->loadProcess("entity/entity-list");
    else if($process_type == "toggle") $this->loadProcess("entity/entity-toggle");
        else if($process_type == "save") $this->loadProcess("entity/entity-save");
        else if($process_type == "remove") $this->loadProcess("entity/entity-remove");
        else 
        $json->sendJSON("Invalid process-type.");
}
?>