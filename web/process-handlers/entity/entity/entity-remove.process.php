<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php

$json = $this->loadClass("json");


$entity_module_link = $_REQUEST['entity_module_link'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name,m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result) { echo "Error in fetching entity details: ".$this->db->error(); return; }

$row = mysqli_fetch_assoc($result);
//define("ENTITY_NAME",$row['entity_name']);
$entity_name = $row['entity_name'];
$entity_id = $row['entity_id'];
$html_id = $row['html_id'];

if(!isset($_REQUEST['entity_value_id'])) $json->sendJSON("entity id is not set");
$entity_value_id=$_REQUEST['entity_value_id'];

$result = $this->db->delete("_entity_value","id",$entity_value_id);
if(!$result){
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove module, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode()." ".$this->db->error());
}

$json->add("reloadModule","#$html_id");
$json->sendJSON("Entity Removed successfully",true);
?>