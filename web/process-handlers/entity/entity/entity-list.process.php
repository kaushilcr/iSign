<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php

$json=$this->loadClass("json");

$entity_module_link = $_REQUEST['entity_module_link'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name, m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result)  $json->sendJSON("Error in fetching entity details: ".$this->db->error());


$row = mysqli_fetch_assoc($result);

$entity = $row['entity_name'];;
$entity_id = $row['entity_id'];;
$html_id = $row['html_id'];

//Get Entity Property Details

$result = $this->db->query("select * from `_property` where `entity_id` = $entity_id");
if(!$result) { echo "Error in fetching property details: ".$this->db->error(); return; }
$property = array();
while($row = mysqli_fetch_assoc($result)) $property[] = $row;


//Get List of column names for query
$sql = "SELECT
  GROUP_CONCAT(DISTINCT
        CONCAT('MAX(IF(pv.name = ''', p.name,''', pv.value, NULL)) AS ''', p.name,'\'') 
  )
FROM _property p, _entity e
WHERE e.`id`= p.`entity_id` AND e.`name`='$entity'";

$result = $this->db->query($sql);
if(!$result){$json->add("error","Error while Executing query: ".$this->db->error());$json->echoJSON();}

$row = mysqli_fetch_array($result);

$subquery = $row[0];
$sql = "SELECT  ev.id,pv.`entity_name`,
        $subquery
FROM `_entity_value` ev
LEFT JOIN (select pv.id, p.id as `property_id`,pv.`entity_value_id`, p.`name`, pv.`value`, e.`name` as `entity_name`
FROM `_property_value` pv, `_property` p, `_entity` e, `_entity_value` ev
WHERE p.`id` = pv.`property_id` AND
      ev.`entity_id` = e.`id` AND
      pv.`entity_value_id` = ev.`id`
ORDER BY pv.`id`) AS pv ON ev.id = pv.entity_value_id
GROUP BY pv.entity_value_id
HAVING entity_name='$entity' ";

//echo $sql;

$innerJson=$this->loadClass("json");

$count = $this->db->query("select count(*) as total from `_entity` e, `_entity_value` ev
WHERE ev.`entity_id` = e.`id`
AND e.`name` = '$entity'");
$row = mysqli_fetch_array($count);
$count = (int)$row[0];
$json->add("recordsTotal",$row[0]);

$json->add("draw",@$_REQUEST['draw']+0);

$colums = array();
foreach($property as $p){
    $n= $p['name'];
    if($p['type']=="date")
        $colums[] = "STR_TO_DATE(MAX(IF(pv.name = '$n', pv.value, NULL)),'%d-%m-%Y')";//STR_TO_DATE(MAX(IF(pv.name = 'start_date', pv.value, NULL)),'%d-%m-%Y')
    else $colums[] = "`$n`";
}


$start = @$_REQUEST['start'];
$length = @$_REQUEST['length'];
$search = @$_REQUEST['search'];
$order = $_REQUEST['order'];
$order_by = $colums[$order[0]['column']];
$order_dir = $order[0]['dir'];
$searchTerm = @$search['value'];
$where = ""; 
if(!empty($searchTerm)){
    $where.= "AND (";
    $whrAry = array();
    foreach($property as $p){
        $whrAry[]= "`".$p['name']."` like '%$searchTerm%' ";
    }
    $where.= implode(" or ",$whrAry);
    //$where.= "`first_name` like '%$searchTerm%' or `last_name` like '%$searchTerm%'";
    $where.=") ";
}

//$result = $this->db->query("select * from customer $where order by 'id' desc LIMIT $start, $length");
//echo "$sql $where LIMIT $start, $length";
$result = $this->db->query("$sql $where ORDER BY $order_by $order_dir LIMIT $start, $length");

if(!$result){
    $json->add("error","Error while Executing query:[$sql $where ORDER BY $order_by $order_dir LIMIT $start, $length] ".$this->db->error());
    $json->echoJSON();
}
$totalFiltered = mysqli_num_rows($result);

$json->add("recordsFiltered",(!empty($searchTerm)) ? $totalFiltered : $count);
$i=0;
while($row = mysqli_fetch_array($result)){

    $data = array();
    foreach($property as $p){
        $index  = $p['name'];
        $data[] = $row["$index"];
    }

    //$data[] =  '<button class="btn btn-default btn-xxs" onclick="viewClient('.$row['id'].')"><i class="fa fa-eye"></i></button>'."&nbsp;".'<button class="btn btn-default btn-xxs" onclick="editClient('.$row['id'].')"><i class="fa fa-pencil"></i></button>'."&nbsp;".'<form action="'.BASE_URL.'customer-remove.process" method="post" style="display:inline-block;"><input type="hidden" name="customer_id" value="'.$row['id'].'"><button class="btn btn-default btn-xxs" onclick="deleteClient('.$row['id'].')"><i class="fa fa-times"></i></button></form>';
    $deleteBtn = '<form action="'.BASE_URL.'entity/entity.process?entity_module_link='.$entity_module_link.'&process-type=remove" method="post" style="display:inline-block;"><input type="hidden" name="entity_value_id" value="'.$row['id'].'"><button class="btn btn-default btn-xxs" type="submit" data-confirm="Are you sure you want to delete ? Action is irreversible and no other confirmation will be asked."><i class="fa fa-times"></i></button></form>';
    $editBtn = '<button class="btn btn-default btn-xxs" onclick="edit'.ucfirst($html_id).'('.$row[0].')"><i class="fa fa-pencil"></i></button>';
    $viewBtn = '<button class="btn btn-default btn-xxs" onclick="view'.ucfirst($html_id).'('.$row[0].')"><i class="fa fa-eye"></i></button>';
    $data[] = $viewBtn.$editBtn.$deleteBtn;

    $innerJson->add($i++,$data);
}
$json->add("data",$innerJson->getArray());
$json->echoJSON();
?>