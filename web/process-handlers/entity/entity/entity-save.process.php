<?php if(! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
$json = $this->loadClass("json");


$entity_module_link = $_REQUEST['entity_module_link'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name, m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result)  $json->sendJSON("Error in fetching entity details: ".$this->db->error());


$row = mysqli_fetch_assoc($result);

$entity = $row['entity_name'];;
$entity_id = $row['entity_id'];;
$html_id = $row['html_id'];

//Get Entity Property Details

$result = $this->db->query("select * from `_property` where `entity_id` = $entity_id");
if(!$result) { echo "Error in fetching property details: ".$this->db->error(); return; }
$property = array();
while($row = mysqli_fetch_assoc($result)) $property[] = $row;


if(!isset($_REQUEST['entity_data'])) $json->sendJSON("entity_data not set.");
if(!isset($_REQUEST['entity_value_id'])) $json->sendJSON("entity_value_id not set.");

$entity_value_id = $_REQUEST['entity_value_id'];
$entity_data = $_REQUEST['entity_data'];

//Validation
foreach($property as $p){
    
    if($p['required']==1){
        if(empty($entity_data[$p['name']])) $json->sendJSON($p['name']." is mandatory field.");
    }
    
}

$this->db->beginTransaction();

if(ctype_digit($entity_value_id)){

    //Update Entry
    foreach($entity_data as $k=>$v){
        $sql_update = "update _property_value set value='$v' where property_id=(select `id` from `_property` p where p.`name`='$k' AND `entity_id`=$entity_id) AND entity_value_id=$entity_value_id";
        //$json->sendJSON($sql_update);
        $result  = $this->db->query($sql_update);
        if(!$result) $json->sendJSON("Error in updating entry:".$this->db->error());
    }

}else{
    //New Entry
    $entity_value_id=$this->db->put("_entity_value",array("entity_id"=>$entity_id));
    if(!$entity_value_id)$json->sendJSON("Error in inserting entry to entity: ".$this->db->error());
    foreach($entity_data as $k=>$v){
        $sql_insert = "insert into _property_value (`value`,`property_id`,`entity_value_id`) values('$v',(select `id` from `_property` p where p.`name`='$k' AND `entity_id`=$entity_id),$entity_value_id)";
        $result  = $this->db->query($sql_insert);
        if(!$result) $json->sendJSON("Error in inserting property: ".$this->db->error());
    }
}
$this->db->commitTransaction();

$json->add("reloadModule","#$html_id");
$json->sendJSON("$entity data saved successfully.",true);
?>

