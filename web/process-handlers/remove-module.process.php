<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
$json = $this->loadClass("json");
if(!isset($_REQUEST['module_id']))  $json->sendJSON("module_id not set");
$module_id = $_REQUEST['module_id'];

$result = $this->db->delete("_module","id",$module_id);
if(!$result) {
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove module, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode());
}
$json->add("reloadModule","#module-list");
$json->sendJSON("Module removed successfully",true);

?>