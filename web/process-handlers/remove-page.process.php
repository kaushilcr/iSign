<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
$json = $this->loadClass("json");
if(!isset($_REQUEST['page_id']))  $json->sendJSON("page_id not set");
$page_id = $_REQUEST['page_id'];

$result = $this->db->delete("_page","id",$page_id);
if(!$result) {
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove page, it is linked to Role/Module");
    }else
        $json->sendJSON("Error in execuing script: (".$this->db->errorCode().")".$this->db->error());
}
$json->add("reloadModule","#pages-list");
$json->sendJSON("Page removed successfully",true);

?>