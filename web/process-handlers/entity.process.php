<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['name'])) $json->sendJSON("title not set");
if(!isset($_REQUEST['description'])) $json->sendJSON("description not set");
if(!isset($_REQUEST['entity_id'])) $json->sendJSON("entity_id not set");


$name = $_REQUEST['name'];
$description = $_REQUEST['description'];
$entity_id = $_REQUEST['entity_id'];

if(empty($name)) $json->sendJSON("Name cannot be empty");

$entity = array();
$entity['name'] = $name;
$entity['description'] = $description;

if(!ctype_digit($entity_id)){
    //Insert here
    $result = $this->db->put("entity",$entity);
    if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());
    $entity_id = $result;

}
else{
    //Update here
    $result = $this->db->update("entity",$entity,array("id"=>$entity_id));
    if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());

}
$json->add("reloadModule","#entity");
$json->sendJSON("Successfully Saved (ID: $entity_id)",true);

?>