<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['title'])) $json->sendJSON("title not set");
if(!isset($_REQUEST['name'])) $json->sendJSON("name not set");
if(!isset($_REQUEST['column_ratio'])) $json->sendJSON("column_ratio not set");
if(!isset($_REQUEST['icon'])) $json->sendJSON("icon not set");
if(!isset($_REQUEST['page_id'])) $json->sendJSON("page_id not set");


$name = trim($_REQUEST['name']);
$title = trim($_REQUEST['title']);
$column_ratio = trim($_REQUEST['column_ratio']);
$icon = trim($_REQUEST['icon']);
$page_id = $_REQUEST['page_id'];

if(empty($title)) $json->sendJSON("Title cannot be empty");
if(empty($name)) $json->sendJSON("Name cannot be empty");

if ( preg_match('/\s/',$name) )  $json->sendJSON("Page Name cannot contain any space");
if(!empty($column_ratio)){
    $columns = explode(":",$column_ratio);
    if(count($columns)<2){
        $json->sendJSON("Invalid Ratio format");
    }
    foreach($columns as $column){
        if(!ctype_digit($column))         $json->sendJSON("Invalid Ratio format");
    }
}

$page = array();
$page['name'] = $name;
$page['title'] = $title;
if(!empty($column_ratio))$page['view'] = $column_ratio;
if(!empty($icon))$page['icon'] = $icon;

$page['sort'] = 1;
if($page_id=="new"){
    $page_id=$this->db->put("_page",$page);
    if(!$page_id) $json->sendJSON("Error in executing script".$this->db->error());
}else{
    $result =$this->db->update("_page",$page,array("id"=>$page_id));
        if(!$result) $json->sendJSON("Error in executing script".$this->db->error());
}
$json->add("reloadModule","#pages-list, #new-page");
$json->sendJSON("Details saved successfully ($page_id)",true);

?>