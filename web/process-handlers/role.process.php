<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['name'])) $json->sendJSON("name not set");
if(!isset($_REQUEST['code'])) $json->sendJSON("code not set");
if(!isset($_REQUEST['role_id'])) $json->sendJSON("role_id not set");

$name = trim($_REQUEST['name']);
$code = trim($_REQUEST['code']);
$role_id = trim($_REQUEST['role_id']);

$enabled = (isset($_REQUEST['enabled'])) ? 1:0;
$visible = (isset($_REQUEST['visible'])) ? 1:0;


if(empty($name)) $json->sendJSON("Title cannot be empty");
if(empty($code)) $json->sendJSON("Name cannot be empty");

if ( preg_match('/\s/',$code) )  $json->sendJSON("role code cannot contain any space");


$role = array();
$role['role_name'] = $name;
$role['role_code'] = $code;
$role['enabled'] = $enabled;
$role['visible'] = $visible;

if($role_id=="new"){
    $role_id=$this->db->put("_role",$role);
    if(!$role_id) $json->sendJSON("Error in executing script".$this->db->error());
}else{
    $result =$this->db->update("_role",$role,array("id"=>$role_id));
        if(!$result) $json->sendJSON("Error in executing script".$this->db->error());
}

$json->sendJSON("Details saved successfully ($role_id)",true);

?>