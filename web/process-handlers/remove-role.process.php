<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
$json = $this->loadClass("json");
if(!isset($_REQUEST['role_id']))  $json->sendJSON("role_id not set");
$role_id = $_REQUEST['role_id'];

$result = $this->db->delete("role","id",$role_id);
if(!$result) {
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove role, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode());
}
$json->add("reloadModule","#role-list");
$json->sendJSON("Role removed successfully",true);

?>