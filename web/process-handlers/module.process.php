<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['title'])) $json->sendJSON("title not set");
if(!isset($_REQUEST['page_id'])) $json->sendJSON("page_id not set");
if(!isset($_REQUEST['module_class'])) $json->sendJSON("module_class not set");
if(!isset($_REQUEST['module_name'])) $json->sendJSON("module_name not set");
if(!isset($_REQUEST['process_name'])) $json->sendJSON("process_name not set");
if(!isset($_REQUEST['module_template'])) $json->sendJSON("module_template not set");


if(!isset($_REQUEST['html_id'])) $json->sendJSON("html_id not set");
if(!isset($_REQUEST['column_number'])) $json->sendJSON("column_number not set");
if(!isset($_REQUEST['sort'])) $json->sendJSON("sort not set");
if(!isset($_REQUEST['module_id'])) $json->sendJSON("module_id not set");
if(!isset($_REQUEST['module_id'])) $json->sendJSON("module_id not set");


$title = trim($_REQUEST['title']);
$page_id = trim($_REQUEST['page_id']);
$module_class = trim($_REQUEST['module_class']);
$module_name = trim($_REQUEST['module_name']);
$process_name = trim($_REQUEST['process_name']);
$module_template = $_REQUEST['module_template'];


$html_id = trim($_REQUEST['html_id']);
$column_number = trim($_REQUEST['column_number']);
$sort = trim($_REQUEST['sort']);
$module_id = trim($_REQUEST['module_id']);


$movable = (isset($_REQUEST['movable'])) ? 1:0;
$collapsible = (isset($_REQUEST['collapsible'])) ? 1:0;
$expandable = (isset($_REQUEST['expandable'])) ? 1:0;
$no_padding = (isset($_REQUEST['no_padding'])) ? 1:0;

if(empty($title)) $json->sendJSON("Title cannot be empty");
if(empty($module_name)) $json->sendJSON("Name cannot be empty");

if ( preg_match('/\s/',$module_name) )  $json->sendJSON("Module name cannot contain any space");

$module = array();

$module['title']=$title;
$module['module_name']=$module_name;
$module['class']=$module_class;
$module['html_id']=$html_id;
$module['movable']=$movable;
$module['collapsible']=$collapsible;
$module['expandable']=$expandable;
$module['no_padding']=$no_padding;
$module['column_number']=$column_number;
$module['process_name']=$process_name;
$module['sort']=$sort;
$module['page_id']=$page_id;

if($module_id=="new"){
    if(!empty($module_template)){
        if($module_template=="List with Form"){
            //Assuming $module_name at most one parent folder 
            $arr = explode("/", $module_name, 3);
            if(isset($arr[1])){
                $folder_name = $arr[0];
                $module_name_ = str_replace(".module","",$arr[1]);
            }else{
                $folder_name = "";
                $module_name_ = str_replace(".module","",$arr[0]);
            }
            if(!file_exists(MODULE.$folder_name))
                mkdir(MODULE.$folder_name);

            //Create Form, List, and detail file for module and process file for list and process form
            //$file = fopen("templates/%module%.module","r");
            $file = file_get_contents("templates/%module%.module.tpl");
            /*var_dump($file);
            var_dump($folder_name);
            var_dump($module_name_);*/
            $content_to_be_written = str_replace("%module%",$module_name_,$file);
            file_put_contents(MODULE.$module_name.".php",$content_to_be_written);
            /*
            $file = file_get_contents("templates/%module%.module.tpl");
            file_put_contents(MODULE.$module.".php",str_replace("%module%",$module_name_,$file));
            
            $file = file_get_contents("templates/%module%.module.tpl");
            file_put_contents(MODULE.$module.".php",str_replace("%module%",$module_name_,$file));
            
            $file = file_get_contents("templates/%module%.module.tpl");
            file_put_contents(MODULE.$module.".php",str_replace("%module%",$module_name_,$file));
            
            */
            
            
        }
    }
}

if($module_id=="new"){
    $module_id=$this->db->put("_module",$module);
    if(!$module_id) $json->sendJSON("Error in executing script".$this->db->error());
}else{
    $result =$this->db->update("_module",$module,array("id"=>$module_id));
    if(!$result) $json->sendJSON("Error in executing script".$this->db->error());
}
$json->add("reloadModule","#module-list, #new-module");
$json->sendJSON("Details saved successfully ($module_id)",true);

?>