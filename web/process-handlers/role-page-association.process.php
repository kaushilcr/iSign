<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['role_id'])) $json->sendJSON("role_id not set");
if(!isset($_REQUEST['page_id'])) $json->sendJSON("page_id not set");

$role_id = $_REQUEST['role_id'];
$page_id = $_REQUEST['page_id'];

$role_page = array();

$role_page['role_id'] = $role_id;
$role_page['page_id'] = $page_id;
if(isset($_REQUEST['link']))
    $result = $this->db->put("_role_page",$role_page);
else if(isset($_REQUEST['unlink']))
    $result = $this->db->query("delete from `_role_page` where `page_id`=$page_id AND `role_id`=$role_id");

if(!$result){
    if($this->db->errorCode()==1062)     $json->sendJSON("Role and page are already associated.(".$this->db->errorCode().")");
    $json->sendJSON("Error in executing script: ".$this->db->error().$this->db->errorCode());
}

$json->sendJSON("Page has been linked/unlinked with role",true);

?>