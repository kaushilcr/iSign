<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['name'])) $json->sendJSON("title not set");
if(!isset($_REQUEST['entity_id'])) $json->sendJSON("entity_id not set");
if(!isset($_REQUEST['type'])) $json->sendJSON("type not set");
if(!isset($_REQUEST['default'])) $json->sendJSON("default not set");
if(!isset($_REQUEST['linked-with'])) $json->sendJSON("linked-with not set");
if(!isset($_REQUEST['field_id'])) $json->sendJSON("field_id not set");



$name = trim($_REQUEST['name']);
$entity_id = $_REQUEST['entity_id'];
$type = $_REQUEST['type'];
$default = $_REQUEST['default'];
$linked_with = $_REQUEST['linked-with'];
$required = (!isset($_REQUEST['required']))?1:0;
$field_id = $_REQUEST['field_id'];


if(empty($name)) $json->sendJSON("Name cannot be empty");
if(empty($entity_id)) $json->sendJSON("Kindly select Entity");

$field = array();
$field['name'] = $name;
$field['entity_id'] = $entity_id;
$field['type'] = $type;
$field['default'] = $default;
$field['linked_with'] = $linked_with;
$field['required'] = $required;

if(!ctype_digit($field_id)){

    //Insert here
    $result = $this->db->put("field",$field);
    if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());
    $field_id = $result;

}
else{
    //Update here
    $result = $this->db->update("field",$field,array("id"=>$field_id));
    if(!$result) $json->sendJSON("Error in executing script: ".$this->db->error());
}
$json->add("reloadModule","#field");
$json->sendJSON("Successfully Saved (ID: $field_id)",true);

?>