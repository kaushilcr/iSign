<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
$json = $this->loadClass("json");
if(!isset($_REQUEST['field_id']))  $json->sendJSON("field_id not set");
$field_id = $_REQUEST['field_id'];

$result = $this->db->delete("field","id",$field_id);
if(!$result) {
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove field, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode());
}
$json->add("reloadModule","#field");
$json->sendJSON("Field removed successfully",true);

?>