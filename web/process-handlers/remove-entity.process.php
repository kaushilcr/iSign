<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php
$json = $this->loadClass("json");
if(!isset($_REQUEST['entity_id']))  $json->sendJSON("entity_id not set");
$entity_id = $_REQUEST['entity_id'];

$result = $this->db->delete("entity","id",$entity_id);
if(!$result) {
    if($this->db->errorCode()==1451){//Foreign Key
        $json->sendJSON("Cannot remove entity, it is linked");
    }else
        $json->sendJSON("Error in execuing script: ".$this->db->errorCode());
}
$json->add("reloadModule","#entity");
$json->sendJSON("Entity removed successfully",true);

?>