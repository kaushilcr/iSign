<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
if(!$this->isAuthorized("ADMIN")){
    $json->sendJSON("Unauthorized access !");
}
if(!isset($_REQUEST['first-name']))   $json->sendJSON("first-name not set"); 
if(!isset($_REQUEST['last-name']))   $json->sendJSON("last-name not set"); 
if(!isset($_REQUEST['role']))   $json->sendJSON("role not set"); 
if(!isset($_REQUEST['username']))   $json->sendJSON("username not set"); 
if(!isset($_REQUEST['password']))   $json->sendJSON("password not set"); 

$first_name = trim($_REQUEST['first-name']);
$last_name = trim($_REQUEST['last-name']);
$role_id  = $_REQUEST['role'];
$username  = $_REQUEST['username'];
$password  = $_REQUEST['password'];


//Little Validation 

if(empty($first_name)) $json->sendJSON("First name cannot be empty");
if(empty($username)) $json->sendJSON("Username cannot be empty");
if(empty($password)) $json->sendJSON("Password cannot be empty");

if(strlen($password)<8 || strlen($password)>15) $json->sendJSON("Password length must be between 8 to 15");
if(!ctype_digit($role_id)) $json->sendJSON("Invalid role id");


//Check for availability 

$count = $this->db->getCount("user","id","username",$username);
$i=0;
if($count==1){
    $json->sendJSON("Username is already assigned !");
}

$user = array();

$user['first_name'] = $first_name;
$user['last_name'] = $last_name;
$user['username'] = $username;
$user['password'] = $password;
$user['role_id'] = $role_id;

$result = $this->db->put("user",$user);

if(!$result)   $json->sendJSON("Error in executing query : ".$this->db->error());

if($this->db->affectedRow==0) $json->sendJSON("No rows affected");


$json->add("reloadModule","#user-list");
$json->add("reloadModule","#add-new-user");
$json->sendJSON("User added successfully",true);

?>