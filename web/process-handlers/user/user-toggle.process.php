<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['user_id'])){
    $json->sendJSON("user_id not set");
}
$user_id = $_REQUEST['user_id'];
if(!ctype_digit($user_id)) $json->sendJSON("Invalid user id");
$sql = "update `_user` set `active`=1-`active` where `id` = $user_id";
$result = $this->db->query($sql);
if(!$result)   $json->sendJSON("Error in executing query : ".$this->db->error());

if($this->db->affectedRow==0) $json->sendJSON("No rows affected");

$json->add("reloadModule","#user");

$json->sendJSON("User status updated successfully.",true);

?>