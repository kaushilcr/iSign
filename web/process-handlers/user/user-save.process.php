<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

if(!isset($_REQUEST['user_id']))   $json->sendJSON("user_id not set"); 
if(!isset($_REQUEST['first-name']))   $json->sendJSON("first-name not set"); 
if(!isset($_REQUEST['last-name']))   $json->sendJSON("last-name not set"); 
if(!isset($_REQUEST['username']))   $json->sendJSON("username not set"); 
if(!isset($_REQUEST['role']))   $json->sendJSON("role not set"); 
if(!isset($_REQUEST['password']))   $json->sendJSON("password not set"); 


$first_name = trim($_REQUEST['first-name']);
$last_name = trim($_REQUEST['last-name']);
$username = trim($_REQUEST['username']);
$role_id  = $_REQUEST['role'];
$password  = $_REQUEST['password'];
$user_id  = $_REQUEST['user_id'];


//Little Validation 

if(empty($first_name)) $json->sendJSON("First name cannot be empty");
if(!empty($password)){
    if(strlen($password)<8 || strlen($password)>15) $json->sendJSON("Password length must be between 4 to 9");
}
if(!ctype_digit($role_id)) $json->sendJSON("Invalid role id");

$user['first_name'] = $first_name;
$user['last_name'] = $last_name;
$user['username'] = $username;
if(!empty($password))
    $user['password'] = md5("&_&_&_".$password."_@_@_@");
$user['role_id'] = $role_id;
if($user_id == "new"){
    $result = $this->db->put("_user",$user);
    if(!$result) $json->sendJSON("Error in executing script :".$this->db->error());

    $json->add("reloadModule","#user");
    $json->sendJSON("User added successfully",true);
}
else
{
    $result = $this->db->update("_user",$user,array("id"=>$user_id));
    if(!$result) $json->sendJSON("Error in executing script :".$this->db->error());

    $json->add("reloadModule","#user");
    $json->sendJSON("User Updated successfully",true);
}
?>