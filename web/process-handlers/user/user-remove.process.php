<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
if(!isset($_REQUEST['user_id'])) $json->sendJSON("user_id not set");
$user_id = $_REQUEST['user_id'];
if(!ctype_digit($user_id)) $json->sendJSON("Invalid User ID");
$result = $this->db->delete("_user","id",$user_id);
$this->db->errorCode();
if($this->db->errorCode() == 1451) $json->sendJSON("User is already linked.");
if(!$result) $json->sendJSON("Error in executing query: ".$this->db->error());
if($this->db->affectedRow<=0) $json->sendJSON("No User deleted",true);
/*if(!defined("FROM_API")){$json->add("reloadModule","#staff");}*/
$json->add("reloadModule","#user");
$json->sendJSON("User deleted.",true);
