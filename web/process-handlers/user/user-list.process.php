<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");

$colums[0] = "`first_name`";
$colums[1] = "`role_name`";


$innerJson=$this->loadClass("json");
$json->add("draw",@$_REQUEST['draw']+0);
$start = $_REQUEST['start'];
$length = $_REQUEST['length'];
$search = $_REQUEST['search'];
$order = $_REQUEST['order'];
$searchTerm = $search['value'];


$order_by = $colums[$order[0]['column']];
$order_dir = $order[0]['dir'];

$result = $this->db->query("select * from `_user`");

if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$total = mysqli_fetch_array($result);


$searchTerm = $search['value'];
$where = "WHERE 1=1 ";
if(!empty($searchTerm)){
    $where.= "AND (";
    $where.= "u.`first_name` like '%$searchTerm%'";
    $where.=") ";
}
$sql_total = "SELECT
        count(u.`id`) as `total`,
            u.`role_id`,
            r.`role_code` as role_code,
            r.`role_name` as role_name,
            DATE_FORMAT(u.last_login,'%b %d, %Y %h:%i %p') as last_login,
            u.`first_name` as first_name,
            u.`last_name` as last_name,
            u.`photo`,
            u.`active`
        FROM `_user` u,`_role` r
           $where AND   u.`role_id` = r.`id` AND
                        r.`visible` = 1
        ORDER BY u.`active` desc";

$result_total = $this->db->query($sql_total);
$row_total = mysqli_fetch_array($result_total);
$json->add("recordsTotal",$row_total['total']);

$searchTerm = $search['value'];
$where = "WHERE 1=1 ";
if(!empty($searchTerm)){
    $where.= "AND (";
    $where.= "u.`first_name` like '%$searchTerm%'";
    $where.=") ";
}
$sql = "SELECT
            u.`id`,
            u.`role_id`,
            r.`role_code` as role_code,
            r.`role_name` as role_name,
            DATE_FORMAT(u.last_login,'%b %d, %Y %h:%i %p') as last_login,
            u.`first_name` as first_name,
            u.`last_name` as last_name,
            u.`photo`,
            u.`active`
        FROM `_user` u,`_role` r
           $where AND   u.`role_id` = r.`id`  AND
                        r.`visible` = 1
        ORDER BY $order_by $order_dir LIMIT $start, $length";
$result = $this->db->query($sql);

if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$totalFiltered = mysqli_num_rows($result);
$json->add("recordsFiltered",$totalFiltered);


$i=0;
while($row = mysqli_fetch_array($result)){
    $data = array();
    $data[] = $row['first_name']." ".$row['last_name'];
    $data[] = $row['role_name'];
    $button_text = (!$row['active']) ? "Enable" : "Disable";
    $button_color = (!$row['active']) ? "btn-success" : "btn-warning"; 
    $enable_disable = '<form action="'.BASE_URL.'user/user.process?process-type=toggle" method="post" style="display:inline-block;"><input type="hidden" value="'.$row['id'].'" name="user_id"><button  class="btn '.$button_color.' btn-xxs" type="submit">'.$button_text.'</button></form>';
    $data[] = ' <button class="btn btn-default btn-xxs" onclick="editUser('.$row['id'].')">Edit</button>&nbsp'.$enable_disable.'
    <form action="'.BASE_URL.'user/user.process?process-type=remove" method="post" style="display:inline-block;"><input type="hidden" name="user_id" value="'.$row['id'].'"><button class="btn btn-default btn-xxs" type="submit" data-confirm="Are you sure you want to delete this user ?" ><i class="fa fa-times"></i></button></form>';

    $innerJson->add($i++,$data);
}


/*

for($i=0;$i<10;$i++){
    $data = array();
    $data[] = $i;
    $data[] = $i*2;
    $data[] = $i*3;
    $data[] = '<button class="btn btn-default btn-xxs"> Some Action</button>';

}
*/
$json->add("data",$innerJson->getArray());
$json->echoJSON();

?>
