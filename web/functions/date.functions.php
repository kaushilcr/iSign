<?php
	function cleanDate($date)
	{
		if($date==="0000-00-00 00:00:00")
		{
			return "";
		}
		if(substr($date,11)==="00:00:00")
		{
			return formatDate(substr($date ,0 ,11));
		}
		return  getDateOnly($date)." ".getTimeOnly($date);
	}
	function getDateOnly($date)
	{
		return formatDate(substr($date ,0 ,11));
	}
	function getTimeOnly($date)
	{
		return formatTime(substr($date ,11));
	}
	function formatDate($date)
	{
		return date("d-M-Y", strtotime($date));
	}
	function formatTime($date)
	{
		return date("g:i A",strtotime($date));
	}
	function formatDatetime($date,$format="Y-m-d H:i:s")
	{
		return date($format,strtotime($date));
	}
	function validateDate($date, $format = 'Y-m-d H:i:s')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	function calculateAge($date)
	{
		$from = new DateTime($date);
		$to  = new DateTime('today');
		return $from->diff($to)->y;
	}

?>