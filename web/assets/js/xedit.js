function xedit(){
        $(".xedit").editable({
            url: BASE_URL+"update-player.process",

            source: [
                {value: "BATSMAN", text: "BATSMAN"},
                {value: "BOWLER", text: "BOWLER"},
                {value: "ALLROUNDER", text: "ALLROUNDER"},
                {value: "WICKETKEEPER", text: "WICKETKEEPER"},
            ],
            success: function(res, newValue) {
                try
                {
                response = $.parseJSON(res);
                }
                catch(err)
                {
                console.log(res);
                if(res=="")
                    return;
                return "Error in Parsing";
                }
                console.log(response.DEBUG);
                if(response.success == false)
                {
                return response.message; //msg will be shown in editable form
                }
            }
        });
        $(".xedit").editable("toggleDisabled");

        $("#edit-button, .edit-button").click(function(e) {
            e.stopPropagation();
            console.log("Edit");
            $(".xedit").editable("toggleDisabled");
            $(".xedit-pic").toggleClass("hide");
            console.log("Edit 2");
        });
    }
$(document).ready(function(){
    xedit();
});
