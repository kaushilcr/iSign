(function ( $ ) {
    $.fn.refreshModule = function(options) {
        var defaults = {
            // These are the defaults.
            target: ".panel-body > div", //Panel-body inside panel
            loader: ".loading", //loading inside panel
            autoRefresh: false,     //UC
            refreshInterval: 5      //UC
        }
        var settings = $.extend({},defaults, options );
        return this.each(function(index) {
            href=$(this).data("href");
            if(href!=undefined){
                var loader = $(this).find(settings.loader);
                var target = $(this).find(settings.target);
                loader.fadeIn();
                $(this).find(".print").fadeOut();
                $.ajax({
                    url: href, 
                    data: settings.data,
                    success: function(result){
                        $(target).html(result);
                        if($.fn.ajaxsubmit2) $(target).find("form:not(.no-ajax)").ajaxsubmit2();
                        if($.fn.showPassword) $(target).find(".showpassword").showPassword();
                        if($.fn.datepicker) $(target).find(".date-picker").datepicker();
                        loader.fadeOut();
                        if(!$(loader.siblings(".minmax").data("target")).hasClass("collapse in")){
                            loader.siblings(".minmax").click();
                        }
                        console.log("Finishin AJX");
                    }
                });
            }
        });
    };
    $.fn.enablePrint = function (){
        return this.each(function(index){
            $(this).find(".print").fadeIn();
        });
    }
}( jQuery ));
