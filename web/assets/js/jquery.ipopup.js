/*================================================================================
 * @name: ipopup by iSign
 * @author: (c)Kaushil Rakhasiya
 * @version: 0.0.1 (Init)
 ================================================================================*/
;(function ( $ ) {
    $.fn.ipopup = function(options) {
        //console.log(options);
        var defaults = {
            // These are the defaults.
            height:"300",
            width: "500",
            position: "fixed"
        }
        var settings = $.extend({},defaults, options );
        return this.each(function(index) {

            target=$(this).data("target");
            if(target!=undefined){
                //var target = $(this).find(settings.target);
                //create overlay
                $("body").append('<div id="ipopup"></div>');
                $("#ipopup").append('<div class="overlay"></div>');

                //create popup (html)
                $("#ipopup").append('<div class="content"></div>');
                //Set css to make it to center
                $("#ipopup .content").css("height",settings.height+"px");
                $("#ipopup .content").css("width",settings.width+"px");

                $("#ipopup .content").css("position",settings.position);
                $("#ipopup .content").css("top","50%");
                $("#ipopup .content").css("left","50%");
                $("#ipopup .content").css("margin-top","-"+(settings.height/2)+ "px");
                $("#ipopup .content").css("margin-left","-"+(settings.width/2)+ "px");
            
                
                
                //assign loader
                $("#ipopup .content").append('<div class="text-center"><i class="fa fa-spinner fa-pulse"></i></div>');

                $.ajax({
                    url: target,
                    data: settings.data,
                    success: function(result){

                        $("#ipopup .content").html(result);
                        if($.fn.ajaxsubmit2) $("#ipopup .content").find("form:not(.no-ajax)").ajaxsubmit2();
                        if($.fn.showPassword) $("#ipopup .content").find(".showpassword").showPassword();
                    }
                }).fail(function(){
                    alert("Failed to load Module");
                });
                //Attach close event when clicked on Overlay
                $("#ipopup .overlay").click(function(e){
                    $(document).unbind("keyup");
                    $("#ipopup").remove();
                });
                //Attach escape key close
                $(document).keyup(function(e) {
                    if (e.keyCode == 27) $("#ipopup .overlay").click();
                });
            }
        });
    };
}( jQuery ));
