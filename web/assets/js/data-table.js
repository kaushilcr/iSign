<script type="text/javascript" src="jquery.dataTables.js"></script>
<script type="text/javascript" src="dataTables.filter.range.js"></script>

    $(document).ready(function() {
        var table = $('#example').DataTable();
          
        /* Add event listeners to the two range filtering inputs */
        $('#min, #max').keyup( function() {
            table.draw();
        } );
    } );
