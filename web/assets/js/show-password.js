(function ( $ ) {
    $.fn.showPassword = function() {
        $(this).each(function (index, input) {
            var $input = $(input);
            $(this).siblings().children(".password-eye").click(function () {
                var change = "";
                if ($(this).hasClass('fa-eye')) {
                    change = "text";
                    $(this).addClass('fa-eye-slash');
                    $(this).removeClass('fa-eye');
                } else {
                    change = "password";
                    $(this).addClass('fa-eye');
                    $(this).removeClass('fa-eye-slash');
                }
                var rep = $("<input type='" + change + "' />")
                .attr("id", $input.attr("id"))
                .attr("name", $input.attr("name"))
                .attr('class', $input.attr('class'))
                .attr('placeholder', $input.attr('placeholder'))
                .val($input.val())
                .insertBefore($input).focus();
                $input.remove();
                $input = rep;
            }).insertBefore($input);
        });
    };
}( jQuery ));