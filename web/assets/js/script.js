function afterPageLoad(){
    $(".movable").sortable({
        handle: '.drag',
        cursor: "move",
        opacity: 0.5,
        connectWith: '.movable',
        placeholder: "ui-sortable-placeholder"
    });
    $(".panel").refreshModule({
        target: ".panel-body > div"
    });
    $(".panel-btn.minmax").click(function(){
        $(this).children("i").toggleClass("maximize");
    });

    $(".panel-btn.reload").click(function(){
        $(this).parents(".panel").refreshModule();
    });
    $(".panel-btn.print").click(function(){
        $(this).parents(".panel").children(".panel-body").find(".printable").print();
    });
    $(".panel-btn.expand").click(function(){
        $(this).children("i").toggleClass("fa-compress");
        $(this).children("i").toggleClass("fa-expand");
        $(this).parents(".panel").toggleClass("expanded");
    });
    $(document).on('mousemove', '.panel-body', function(e) { var a=$(this).offset().top+$(this).outerHeight()-16,b=$(this).offset().left+$(this).outerWidth()-16;$(this).css({cursor:e.pageY>a&&e.pageX>b?"nw-resize":""}); })
}
$(document).ready(function(){
    afterPageLoad();
    $(".panel-btn.expand").click(function(){
        $(this).children("i").toggleClass("fa-compress");
        $(this).children("i").toggleClass("fa-expand");
        $(this).parents(".panel").toggleClass("expanded");
    });
});
$(document).ready(function(){
    afterPageLoad();

    $( document ).ajaxStart(function() {
        NProgress.start();
    });
    $( document ).ajaxStop(function() {
        NProgress.done();
    });
});

