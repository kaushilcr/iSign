(function ( $ ) {
    $.fn.ajaxsubmit2 = function() {
        var result = $(this).data("result");

        $(this).ajaxForm({ 
            // dataType identifies the expected content type of the server response 
            // dataType:  'json', 
            beforeSubmit: function(){
                $('#loaderimg').show();
                $('form button[type=submit]').attr('disabled','disabled');
                console.log(this);
                console.log('Data is being uploaded. Please wait and do not refresh');
            },
            beforeSerialize: function($a,b){

                // var isHtml = $a.find("button[type=submit]:focus").data("is-html");
                var confirmbox = $a.find("button[type=submit]:focus").data("confirm");


                if(confirmbox)
                    return confirm(confirmbox);

                if(typeof tinyMCE==='undefined');
                else
                    tinyMCE.triggerSave();
            },
            // success identifies the function to invoke when the server response 
            // has been received 
            success:   function(data){

                var IS_JSON = true;
                try
                {
                    data = $.parseJSON(data);
                }
                catch(err)
                {
                    IS_JSON = false;
                }                
                $('form button[type=submit]').removeAttr('disabled');
                $('#loaderimg').hide();

                if(!IS_JSON)
                {
                    if($("body").data("is-html")){
                        $('.servermessage').html(data);
                    }else{
                        $.notify('Error in Parsing Response',"error");
                    }
                    console.log(data);
                }
                else if(data.success){
                    $.notify(data.message,"success");
                    if(data.html){
                        $(result).html(data.html);
                        return;
                    }
                    if(data.callback){
                        if(data.args){
                            window[data.callback](data.args);

                        }else{
                            window[data.callback]();
                        }
                    }
                    console.log(data);
                    if(data.redirect){
                        window.location.href = data.redirect;
                    }
                    if(data.reload){
                        location.reload();
                    }
                    if(data.reloadModule){
                        $(data.reloadModule).refreshModule();
                    }
                }
                else
                {
                    $.notify(data.message,"error");
                    console.log(data);
                }
                if(data.closePopup){
                    $("#ipopup .overlay").click();
                }
            }
        }); 
    };
}( jQuery ));

$(document).ready(function(){
    $("form:not(.no-ajax)").ajaxsubmit2();
}); 
