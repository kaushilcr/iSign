<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
session_write_close();
?>
<button class="btn btn-danger btn-circle" onclick="newcustomer();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table table striped" id="customer-list">
    <thead>
        <th>Customer Name</th>
        <th> Contact No</th>
        <th>Profssion</th>
        <th>Company Name </th>
        <th>Intrested In</th>
        <th>Action</th>
    </thead>
</table>
<div style="height:30px"></div>
<script>
    function newcustomer(){
        $("#customer").refreshModule({
            data: {
                customer_id: "new"
            }
        })
    }
    function viewcustomer(customer_id){
        $("#customer").refreshModule({
            data: {
                customer_id: customer_id,
                type:"view"
            }
        })
    }
    function editcustomer(customer_id){
        $("#customer").refreshModule({
            data: {
                customer_id: customer_id,
                type:"edit"
            }
        })
    }

    $('#customer-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>customer/customer.process?process-type=list",
        "columnDefs": [ { orderable: false, targets: [2] } ],
        "language": {
            "lengthMenu": "_MENU_"
        }
    });
</script>