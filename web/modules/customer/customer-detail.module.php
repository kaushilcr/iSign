<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
session_write_close();
?>
<?php
$customer_id= $_REQUEST['customer_id'];
if(!ctype_digit($customer_id)){
    echo "Invalid Customer Id";
    return;
}
$sql = "select * FROM customer where id=$customer_id";
$result_customer = $this->db->query($sql);
$row_customer = mysqli_fetch_array($result_customer);
?>
<div class="printable">
    <style>
        th,td{
            padding:2px 5px 2px 5px;
        }
    </style>
    <table style="undefined;table-layout:fixed;width: 800px;">
        <colgroup>
            <col style="width:800px;">           
        </colgroup>
        <tr>
            <th style="text-align:right;font-size:18px;">Customer Id.:<div style="font-weight:normal;display:inline-block;"><?php echo $row_customer['id'];?></div></th>
        </tr>
    </table>
    <table style="undefined;table-layout:fixed;width:800px;">
        <colgroup>
            <col style="width:800px">
        </colgroup>
        <tr>
            <th style="border:1px solid #000;padding:7px;font-size:18px;text-align:center;">Customer Receipt</th>
        </tr>
    </table>
    <table style="undefined;table-layout:fixed;width:800px;margin-top:10px;">
        <tr>          
            <th style="border:1px solid #000;padding:20px;"rowspan="4">Name:<div style="font-weight:normal;display:inline-block;"><?php echo $row_customer['customer_name'];?></div><br>Address:<div style="font-weight:normal;display:inline-block;"><?php echo $row_customer['address'];?></div><br>Contact No:<div style="font-weight:normal;display:inline-block;"><?php echo $row_customer['contact_no'];?></div></th>
        </tr>
    </table>
    <table style="undefined;table-layout:fixed;width:800px;margin-top:20px;">
        <colgroup>
            <col style="width:20px">
            <col style="width:20px">
            <col style="width:20px">
            <col style="width:20px">
            <col style="width:20px">
            <col style="width:20px">
        </colgroup>
        <tr>
            <th style="border:1px solid #000;text-align:center;padding:10px;">Profession</th>
            <th style="border:1px solid #000;text-align:center;padding:10px;">Company Name</th>
            <th style="border:1px solid #000;text-align:center;padding:10px;">No Of Family Members</th>
            <th style="border:1px solid #000;text-align:center;padding:10px;">Annual Income</th>
            <th style="border:1px solid #000;text-align:center;padding:10px;">Budget</th>
            <th style="border:1px solid #000;text-align:center;padding:10px;">IntrestedIn</th>
        </tr>
        <tr>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['profession'];?></td>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['company_name'];?></td>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['family_members'];?></td>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['annual_income'];?></td>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['budget'];?></td>
            <td style="border:1px solid #000;text-align:center;"><?php echo $row_customer['intrested_in'];?></td>
        </tr>
    </table>

</div>
<script>
    $("#customer").enablePrint();
</script>