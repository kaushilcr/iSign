<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
session_write_close();
?>
<?php
if(!isset($_REQUEST['customer_id'])){
    $customer_id ="";
    $this->loadModule("customer/customer-list");
    return;
}else{
    $customer_id = $_REQUEST['customer_id'];
    if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
        $this->loadModule("customer/customer-detail");
    else
        $this->loadModule("customer/customer-form");
    return;
}
?>