<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
session_write_close();
?>
<?php
$customer_id = $_REQUEST['customer_id'];
if(ctype_digit($customer_id)){
    //site summary
    $sql = "select * from `customer` where `id`=$customer_id";
    $result = $this->db->query($sql);
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    $customer = mysqli_fetch_array($result);

    $customer_detail = array();
    while($row = mysqli_fetch_array($result)) $customer_detail[] = $row;
}
else{
    $customer_id = "new";
}
?>
<form  action="<?php echo BASE_URL?>customer/customer.process" mehtod="post">
    <input type="hidden"  name="customer_id" value="<?php echo $customer_id?>">

    <h3 class="text-center"><b>Customer Detail </b></h3>
    <hr style="width:150px;text-align:center;">

    <div class="row">
        <div class="col-sm-4">
            <label for="">Customer Name</label>
            <input type="text" name="customer_name" class="form-control input-xs" placeholder="Name" value="<?php echo @$customer['customer_name']?>">
        </div>
        <div class="col-sm-4">
            <label for=""> Address</label>
            <textarea rows="2" class="form-control input-xs" placeholder="Address" name="address"><?php echo @$customer['address']?></textarea>
        </div>
        <div class="col-sm-4">
            <label for=""> Contact No</label>
            <input type="text" name="contact_no" class="form-control input-xs" placeholder="Contact No" value="<?php echo @$customer['contact_no']?>">
        </div>
    </div>

    <div class="row" style="height:20px;"></div>

    <div class="row">
        <div class="col-sm-4">
            <label for="profession">Profession:</label>
            <input type="text" name="profession" placeholder="Profession" class="form-control input-sm" value="<?php echo @$customer['profession']?>">
        </div>
        <div class="col-sm-4">
            <label for="profession">Company Name:</label>
            <input type="text" name="company_name" placeholder="Company Name" class="form-control input-sm" value="<?php echo @$customer['company_name']?>">
        </div>
    </div> 

    <div class="row" style="height:20px;"></div>

    <div class="row">
        <div class="col-sm-4">
            <label for="profession">No of Family Members:</label>
            <input type="text" name="family_members" placeholder="No of family members" class="form-control input-sm" value="<?php echo @$customer['family_members']?>">
        </div>
    </div>

    <div class="row" style="height:20px;"></div>

    <div class="row">
        <div class="col-sm-4">
            <label for="profession">Annual Income</label>
            <input type="text" name="annual_income" placeholder="Annual Income" class="form-control input-sm" value="<?php echo @$customer['annual_income']?>">
        </div>
        <div class="col-sm-4">
            <label for="profession">Budget</label>
            <input type="text" name="budget" placeholder="Budget" class="form-control input-sm" value="<?php echo @$customer['budget']?>">
        </div>
        <div class="col-sm-4">
            <label for="profession">Intrested In</label>
            <input type="text" name="intrested_in" placeholder="Intrested In" class="form-control input-sm" value="<?php echo @$customer['intrested_in']?>">
        </div>
    </div>

    <hr style="width:250px;text-align:center;">
    <div class="row">
        <div class="col-sm-12 text-center">
            <?php if($customer_id == "new"){ ?>
            <button type="submit" class="btn btn-xs btn-success">Add</button>
            <?php } else {  ?>
            <button type="submit" class="btn btn-xs btn-success">Update</button>
            <?php } ?>     
        </div>
    </div>
</form>