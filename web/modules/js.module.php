<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<script>
    var BASE_URL = "<?php echo BASE_URL?>";
</script>
<?php
    $loadAry = array();
if(defined("AJAXSUBMIT") && AJAXSUBMIT)
{
    $loadAry[] = "jquery.form.min";
    $loadAry[] = "ajaxsubmit";
}

if(defined("NOTIFY") && NOTIFY){
    $loadAry[] = "notify.min";
}

if(defined("DROPZONE") && DROPZONE) {
    $loadAry[] = "dropzone";
}

if(defined("SHOW_PASSWORD") && SHOW_PASSWORD){
    $loadAry[] = "show-password";
}

if(defined("DIGIT_ONLY") && DIGIT_ONLY)
{
    $loadAry[] = "digit-only";
}

if(defined('XEDITABLE') && XEDITABLE){
    $loadAry[] = 'bootstrap-editable.min';
    $loadAry[] = 'moment.min';
    $loadAry[] = 'xedit';		
}

if(defined('SORTABLE') && SORTABLE){
    $loadAry[] = 'jquery.sortable';	
    $loadAry[] = 'jquery.sortable.init';	
}


if(defined('PRETTYPHOTO_JS') && PRETTYPHOTO_JS){
    $loadAry[] = 'jquery.prettyPhoto';		
    $loadAry[] = 'photo';
}

$loadAry = array_unique ($loadAry);

foreach($loadAry as $js)
    $this->loadJS($js);
?>