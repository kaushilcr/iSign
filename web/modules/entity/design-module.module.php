<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
$result = $this->db->get("_module");
if(!$result) { echo "Error in fetch module list: ".$this->db->error();return; }
$module_list  = array();
while($row = mysqli_fetch_assoc($result)) $module_list[] = $row;

$result = $this->db->get("_entity");
if(!$result) { echo "Error in fetch entity list: ".$this->db->error();return; }
$entity_list  = array();
while($row = mysqli_fetch_assoc($result)) $entity_list[] = $row;

?>
<form action="entity/design-module.process" method="post">
    <div class="row">
        <div class="col-sm-6">
            <label>Module</label>
            <select name="module_id" class="form-control input-sm" required>
                <option></option>
                <?php foreach ($module_list as $module){ ?> 
                <option value="<?php echo $module['id']; ?>"><?php echo $module['title']; ?></option>
                <?php }?>
            </select>  
        </div>
        <div class="col-sm-6">
            <label>Entity</label>
            <select name="entity_id" class="form-control input-sm _entity_select" required>
                <option></option>
                <?php foreach ($entity_list as $entity){ ?> 
                <option value="<?php echo $entity['id']; ?>"><?php echo $entity['name']; ?></option>
                <?php }?>
            </select>
        </div>

    </div>
    <br>
<!--    <div class="row">
        <div class="col-sm-4">
            <span class="h4">List View</span>
        </div>
        <div class="col-sm-8">
            <input type="radio" name="list_view_type" value="default"> Default
            <input type="radio" name="list_view_type" value="manual"> Manual
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <span class="h4">Form View</span>
        </div>
        <div class="col-sm-8">
            <input type="radio" name="form_view_type" value="default"> Default
            <input type="radio" name="form_view_type" value="manual"> Manual
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <span class="h4">Detail View</span>
        </div>
        <div class="col-sm-8">
            <input type="radio" name="detail_view_type" value="default"> Default
            <input type="radio" name="detail_view_type" value="manual"> Manual
        </div>
    </div>
    <br>-->
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#list_view" data-toggle="tab">List</a></li>
        <li role="presentation"><a href="#view_view" data-toggle="tab">View</a></li>
        <li role="presentation"><a href="#form_view" data-toggle="tab">Form</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="list_view">
            List
        </div>
        <div class="tab-pane" id="view_view">
            View
        </div>
        <div class="tab-pane" id="form_view">
            Form
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-primary btn-xs" type="submit">Process</button>
    </div>
</form>
<script>
    $("._entity_select").change(function(){
        var entity_id = $(this).val();
        /* $.ajax({
            url:"<?php echo BASE_URL?>entity/design-module.process?process-type=getproperty",
            success: function(response){

            }

        })*/
    });
</script>