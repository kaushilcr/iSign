<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php $entity_id = "new"; ?>
<form action="<?php echo BASE_URL?>entity/entity.process?process-type=save" method="post" data-loader="">
    <input type="hidden" name="entity_id" value="<?php echo @$entity_id; ?>">
    <div class="form-group">
        <label>Entity Name</label>
        <input type="text" class="form-control input-sm" placeholder="Enter entity name" name="entity_name">
    </div>
    Properties <br><br>
    <table id="property-list" class="table table-condensed">
        <tr>
            <th>
                <input type="text" class="form-control input-sm" name="property_name[]">
            </th>
            <th>
                <input type="text" class="form-control input-sm" name="property_display_name[]">
            </th>
            <th>
                <select name="property_type[]" class="form-control input-sm">
                    <option value="text">text</option>
                    <option value="textarea">textarea</option>
                    <option value="date">date</option>
                </select>
            </th>
            <th>
                <button class="btn btn-danger btn-xxs" type="button" onclick="removeProperty(this)"><i class="fa fa-times"></i></button>
            </th>
        </tr>
    </table>
    <button class="btn btn-link btn-sm" type="button" onclick="addProperty()">Add</button>

    <div class="text-center">
        <button class="btn btn-primary btn-xs" type="submit">Save</button>
        <button class="btn btn-info btn-xs" type="reset">Reset</button>
        <button class="btn btn-default btn-xs" type="button" onclick='$("#entity").refreshModule()'>Close</button>
    </div>
</form>
<script>
    $(document).ready(function(){
        window.propertyTemplate = $("#property-list tr:first").html();
        window.propertyTemplate = "<tr>"+window.propertyTemplate+"</tr>";
        $("#property-list tr:first").remove();
        addProperty();
    });
    function addProperty(){
        $("#property-list").append(propertyTemplate);
    }
    function removeProperty(e){
        $(e).parents("tr").remove();
    }
</script>