<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<?php 
//Get Entity Details
$entity_module_link = $_REQUEST['entity_module_link'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name,m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result) { echo "Error in fetching entity details: ".$this->db->error(); return; }

$row = mysqli_fetch_assoc($result);
//define("ENTITY_NAME",$row['entity_name']);
$entity_name = $row['entity_name'];
$entity_id = $row['entity_id'];
$html_id = $row['html_id'];


//Get Entity Property Details

$result = $this->db->query("select * from `_property` where `entity_id` = $entity_id");
if(!$result) { echo "Error in fetching property details: ".$this->db->error(); return; }
$property = array();
while($row = mysqli_fetch_assoc($result)) $property[] = $row;

?>
<button class="btn btn-danger btn-circle new-btn" onclick="new<?php echo ucfirst($html_id);?>();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped" id="<?php echo $html_id; ?>-list" width="100%">
    <thead>
        <tr>
            <?php $totalColumn = 0;foreach($property as $p){?>
            <th><?php echo $p['display_name'] ?></th>
            <?php $totalColumn++;}?>
            <th>Action</th>
        </tr>
    </thead>
</table>
<div style="height:30px"></div>
<script>
    function new<?php echo ucfirst($html_id);?>(){
        $("#<?php echo $html_id; ?>").refreshModule({
            data: {
                entity_value_id: "new"
            }
        })
    }
    function edit<?php echo ucfirst($html_id);?>(entity_value_id){
        $("#<?php echo $html_id; ?>").refreshModule({
            data: {
                entity_value_id: entity_value_id,
                type:"edit"
            }
        })
    }
    function view<?php echo ucfirst($html_id);?>(entity_value_id){
        $("#<?php echo $html_id; ?>").refreshModule({
            data: {
                entity_value_id: entity_value_id,
                type:"view"
            }
        })
    }
 
    $('#<?php echo $html_id; ?>-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>entity/entity.process?entity_module_link=<?php echo $entity_module_link?>&process-type=list",
        "columnDefs": [ { orderable: false, targets: [<?php echo $totalColumn;?>] } ],
        "language": {
            "lengthMenu": "_MENU_"  
        }
    }).on("draw.dt",function(){
        $("form:not(.no-ajax)").ajaxsubmit2();
    });
</script>
