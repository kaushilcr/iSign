<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
$entity_module_link = $_REQUEST['entity_module_link'];



if(!isset($_REQUEST['entity_value_id'])){
    $this->loadModule("entity/entity/entity-list");
    return;
}else{
    $entity_value_id = $_REQUEST['entity_value_id'];
    if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
        $this->loadModule("entity/entity/entity-detail");
    else
        $this->loadModule("entity/entity/entity-form");
    return;
}
?>