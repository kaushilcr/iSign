<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
//Get Entity Details
$entity_module_link = $_REQUEST['entity_module_link'];
$entity_value_id = $_REQUEST['entity_value_id'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name,m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result) { echo "Error in fetching entity details: ".$this->db->error(); return; }

$row = mysqli_fetch_assoc($result);
//define("ENTITY_NAME",$row['entity_name']);
$entity_name = $row['entity_name'];
$entity_id = $row['entity_id'];
$html_id = $row['html_id'];


//Get Entity Property Details

$result = $this->db->query("select * from `_property` where `entity_id` = $entity_id");
if(!$result) { echo "Error in fetching property details: ".$this->db->error(); return; }
$property = array();
while($row = mysqli_fetch_assoc($result)) $property[] = $row;


$sql = "SELECT
      GROUP_CONCAT(DISTINCT
         CONCAT('MAX(IF(pv.name = ''', p.name,''', pv.value, NULL)) AS ''', p.name,'\'') 
      )
    FROM _property p, _entity e
    WHERE e.`id`= p.`entity_id` AND e.`name`='$entity_name'";

$result = $this->db->query($sql);
if(!$result){$json->add("error","Error while Executing query: ".$this->db->error());$json->echoJSON();}

$row = mysqli_fetch_array($result);

$subquery = $row[0];
$sql = "SELECT  ev.id,pv.`entity_name`,
            $subquery
    FROM `_entity_value` ev
    LEFT JOIN (select pv.id, p.id as `property_id`,pv.`entity_value_id`, p.`name`, pv.`value`, e.`name` as `entity_name`
    FROM `_property_value` pv, `_property` p, `_entity` e, `_entity_value` ev
    WHERE p.`id` = pv.`property_id` AND
          ev.`entity_id` = e.`id` AND
          pv.`entity_value_id` = ev.`id`
    ORDER BY pv.`id`) AS pv ON ev.id = pv.entity_value_id
    GROUP BY pv.entity_value_id
    HAVING entity_name='$entity_name' AND ev.`id`='$entity_value_id'";

$result = $this->db->query($sql);
if(!$result){ echo "Error while getting data: ".$this->db->error(); return;}
$entity_value = mysqli_fetch_assoc($result);

?>
<div class="printable">
    <table class="table table-condensed table-hover table-striped">
        <?php foreach($property as $p){ ?>
        <tr>
            <td><?php echo $p['display_name']?></td>
            <td><?php echo @$entity_value[$p['name']];?></td>
        </tr>
        <?php }?>
    </table>
</div>
<div class="text-center">
    <button class="btn btn-default btn-xs" type="button" onclick='$("#<?php echo $html_id?>").refreshModule()'>Close</button>
</div>
<script>
    $("#<?php echo $html_id?>").enablePrint();
</script>