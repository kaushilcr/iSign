<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<?php 
function getOptions($id,&$options,&$init){
    $result = $init->db->query("select * from `_property_option` where `property_id`=$id order by `sort`");
    if(!$result){
        $options[] = "Error in parsing.";
        return;
    }
    while($row = mysqli_fetch_assoc($result)){
        $options[] = $row['option'];
    }
}
?>
<?php
$entity_module_link = $_REQUEST['entity_module_link'];
$result = $this->db->query("select em.id,e.name as entity_name,e.`id` as entity_id, m.title as module_name,m.html_id from `_entity_module` em, `_entity` e, `_module` m where em.`entity_id` = e.`id` AND em.`module_id` = m.`id` AND em.`id`=$entity_module_link");
if(!$result) { echo "Error in fetching entity details: ".$this->db->error(); return; }

$row = mysqli_fetch_assoc($result);
//define("ENTITY_NAME",$row['entity_name']);
$entity_name = $row['entity_name'];
$entity_id = $row['entity_id'];
$html_id = $row['html_id'];


//Get Entity Property Details

$result = $this->db->query("select * from `_property` where `entity_id` = $entity_id");
if(!$result) { echo "Error in fetching property details: ".$this->db->error(); return; }
$property = array();
while($row = mysqli_fetch_assoc($result)) $property[] = $row;


$entity_value_id = $_REQUEST['entity_value_id'];
if(ctype_digit($entity_value_id)){
    //Get Data for Edit.


    //Get List of column names for query
    $sql = "SELECT
      GROUP_CONCAT(DISTINCT
         CONCAT('MAX(IF(pv.name = ''', p.name,''', pv.value, NULL)) AS ''', p.name,'\'') 
      )
    FROM _property p, _entity e
    WHERE e.`id`= p.`entity_id` AND e.`name`='$entity_name'";

    $result = $this->db->query($sql);
    if(!$result){$json->add("error","Error while Executing query: ".$this->db->error());$json->echoJSON();}

    $row = mysqli_fetch_array($result);

    $subquery = $row[0];
    $sql = "SELECT  ev.id,pv.`entity_name`,
            $subquery
    FROM `_entity_value` ev
    LEFT JOIN (select pv.id, p.id as `property_id`,pv.`entity_value_id`, p.`name`, pv.`value`, e.`name` as `entity_name`
    FROM `_property_value` pv, `_property` p, `_entity` e, `_entity_value` ev
    WHERE p.`id` = pv.`property_id` AND
          ev.`entity_id` = e.`id` AND
          pv.`entity_value_id` = ev.`id`
    ORDER BY pv.`id`) AS pv ON ev.id = pv.entity_value_id
    GROUP BY pv.entity_value_id
    HAVING entity_name='$entity_name' AND ev.`id`='$entity_value_id'";

    $result = $this->db->query($sql);
    if(!$result){ echo "Error while getting data: ".$this->db->error(); return;}
    $entity_value = mysqli_fetch_assoc($result);

}else{
    $entity_value_id="new";
}
?>
<form action="<?php echo BASE_URL?>entity/entity.process?entity_module_link=<?php echo $entity_module_link?>&process-type=save" method="post">
    <input type="hidden" name="entity_value_id" value="<?php echo @$entity_value_id; ?>">

    <div class="row">
        <?php foreach($property as $p){ ?>
        <div class="col-sm-6">      
            <label for="<?php echo $html_id."_".$p['name']?>"><?php echo $p['display_name']; ?></label>
            <?php if($p['type']=="text"){?>
            <input type="text" name="entity_data[<?php echo $p['name']?>]" id="<?php echo $html_id."_".$p['name']?>" class="form-control input-sm" value="<?php echo @$entity_value[$p['name']]; ?>" <?php echo ($p['required']) ? "required" : "";?> >
            <?php }else if ($p['type']=="textarea"){ ?>
            <textarea name="entity_data[<?php echo $p['name']?>]" id="<?php echo $html_id."_".$p['name']?>" class="form-control input-sm" rows="4" <?php echo ($p['required']) ? "required" : "";?>><?php echo @$entity_value[$p['name']]; ?></textarea>
            <?php }else if ($p['type']=="date"){ ?>
            <input type="text" name="entity_data[<?php echo $p['name']?>]" id="<?php echo $html_id."_".$p['name']?>" class="form-control input-sm date-picker" value="<?php echo @$entity_value[$p['name']]; ?>" data-date-format="dd-mm-yyyy" <?php echo ($p['required']) ? "required" : "";?>>
            <?php }else if ($p['type']=="select"){ $options = array(); getOptions($p['id'],$options,$this);?>
            <select name="entity_data[<?php echo $p['name']?>]" id="<?php echo $html_id."_".$p['name']?>" class="form-control input-sm" <?php echo ($p['required']) ? "required" : "";?>>
                <option value="">Kindly select</option>
                <?php foreach($options as $option){?>
                <option value="<?php echo $option?>" <?php echo ($option==@$entity_value[$p['name']]) ? "selected" : "";?> ><?php echo $option?></option>
                <?php } ?>
            </select>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <br>
    <div class="form-group text-center">
        <button class="btn btn-primary btn-xs" type="submit">Save</button>
        <button class="btn btn-info btn-xs" type="reset">Reset</button>
        <button class="btn btn-default btn-xs" type="button" onclick='$("#<?php echo $html_id?>").refreshModule()'>Close</button>
    </div>
</form>
