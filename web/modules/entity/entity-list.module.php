<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<?php 


?>
<button class="btn btn-danger btn-circle" onclick="newEntity();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped" id="entity-list" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>No. of Property</th>
            <th>Total</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<div style="height:30px"></div>
<script>
    function newEntity(){
        $("#entity").refreshModule({
            data: {
                entity_id: "new"
            }
        })
    }
    function editEntity(entity_id){
        $("#entity").refreshModule({
            data: {
                entity_id: entity_id,
                type:"edit"
            }
        })
    }

    $('#entity-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>entity/entity.process?process-type=list",
        "columnDefs": [ { orderable: false, targets: [3] } ],
        "language": {
            "lengthMenu": "_MENU_"
        }
    }).on("draw.dt",function(){
        $("form:not(.no-ajax)").ajaxsubmit2();
    });

</script>
