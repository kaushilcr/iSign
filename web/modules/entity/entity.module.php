<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(isset($_REQUEST['entity_module_link'])){
    $entity_module_link = $_REQUEST['entity_module_link'];
    $this->loadModule("entity/entity/entity");
}
else{
    if(!isset($_REQUEST['entity_id'])){
        $entity_id ="";
        $this->loadModule("entity/entity-list");
        return;
    }else{
        $entity_id = $_REQUEST['entity_id'];
        if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
            $this->loadModule("entity/entity-detail");
        else
            $this->loadModule("entity/entity-form");
        return;
    }
}
?>