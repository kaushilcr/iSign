<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
/* 3 States for staff_id

Not set  =  List 
"new" = New Form
"edit" = Edit Entry

*/

if(!isset($_REQUEST['user_id'])){
    $user_id ="";
    $this->loadModule("user/user-list");
    return;
}else{
    $user_id = $_REQUEST['user_id'];
    if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
        $this->loadModule("user/user-detail");
    else
        $this->loadModule("user/user-form");
    return;
}
?>