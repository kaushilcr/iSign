<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php if(!$this->session->isLoggedIn()){
    echo "Unauthorized";
    return;
}?>
<?php
//List of Roles
session_write_close();

$sql = "select * from `_role` where `enabled`=1 and `visible`=1";
$result = $this->db->query($sql);
if(!$result) die($this->db->error());
$roles = array();
while($row = mysqli_fetch_array($result)){
    $role_id = $row['id'];
    $roles["$role_id"] = $row['role_name'];
}
session_write_close();
?>
<?php
$user_id= $_REQUEST['user_id'];
if(ctype_digit($user_id)){
    $result = $this->db->query("SELECT
            u.`id`,
            u.`role_id`,
            r.`role_code` as role_code,
            r.`role_name` as role_name,
            DATE_FORMAT(u.last_login,'%b %d, %Y %h:%i %p') as last_login,
            u.`first_name` as first_name,
            u.`last_name` as last_name,
            u.`username`,
            u.`photo`,
            u.`active`
        FROM `_user` u,`_role` r
        WHERE
            u.`role_id` = r.`id` AND 
            u.`id`=$user_id");
    if(!$result){
        echo "Error in fetching vendor details: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "Invalid User ID";
        return;
    }
    $user = mysqli_fetch_array($result);
}
else
    $user_id = "new";
?>
<form action="<?php echo BASE_URL?>user/user.process?process-type=save" method="post" data-loader="">
    <input type="hidden" name="user_id" value="<?php echo @$user_id; ?>">
    <div class="form-group">
        <label for="first_name_id">Name</label>
        <div class="row">
            <div class="col-xs-6">             
                <input type="text" name="first-name" id="first_name_id" class="form-control input-sm" value="<?php echo @$user['first_name']?>" placeholder="First Name" required>
            </div>
            <div class="col-xs-6">
                <input type="text" name="last-name"  id="last_name_id" class="form-control input-sm" value="<?php echo @$user['last_name']; ?>" placeholder="Last Name">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="username">Username</label>       
                <input type="text" name="username" id="username" class="form-control input-sm" value="<?php echo @$user['username'] ?>" placeholder="Username" required>
            </div>
            <div class="col-xs-6">
                <label for="password">
                    Password
                    <i class="fa fa-eye password-eye pull-right"></i>
                </label>
                <input type="password" name="password"  id="password" class="form-control input-sm showpassword" placeholder="Password">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="roleId">Role</label>
        <select name="role" id="roleId" class="form-control input-sm">
            <?php foreach($roles as $role_id=>$role) { ?>
            <option value="<?php echo @$role_id ?>"<?php if($role_id == @$user['role_id']) echo "selected"; ?>><?php echo @$role?></option>
            <?php } ?> 
        </select>
    </div>
    
    <div class="form-group text-center">
        <button class="btn btn-primary btn-xs" type="submit">Save</button>
        <button class="btn btn-info btn-xs" type="reset">Reset</button>
        <button class="btn btn-default btn-xs" type="button" onclick='$("#user").refreshModule()'>Close</button>
    </div>
</form>
