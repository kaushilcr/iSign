<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<button class="btn btn-danger btn-circle" onclick="newUser();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped" id="user-list" width="100%">
    <thead>
        <tr>
            <th>Full Name</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<div style="height:30px"></div>
<script>
    function newUser(){
        $("#user").refreshModule({
            data: {
                user_id: "new"
            }
        })
    }
    function editUser(user_id){
        $("#user").refreshModule({
            data: {
                user_id: user_id,
                type:"edit"
            }
        })
    }

    $('#user-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>user/user.process?process-type=list",
        "columnDefs": [ { orderable: false, targets: [2] } ],
        "language": {
            "lengthMenu": "_MENU_"
        }
    }).on("draw.dt",function(){
        $("form:not(.no-ajax)").ajaxsubmit2();
    });

</script>
