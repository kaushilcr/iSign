<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>

<span id="inlineCalc"></span> 

<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.plugin.js"></script> 
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.calculator.js"></script>


<script>
    //$.calculator.addKeyDef(['@E', '#erase', $.calculator.control, $.calculator._erase, 'erase', 'ERASE', 27, 'Esc']);
    
    //'@X', '#close', plugin.control, plugin._close, 'close', 'CLOSE', 27, 'Esc'
    $("#inlineCalc").calculator({layout:  
                                 ['MC_7_8_9_+BS', 
                                  'MR_4_5_6_-@E', 
                                  'MS_1_2_3_*@U',// + $.calculator.ERASE,
                                  'M+_0_._=_/']});

</script>
