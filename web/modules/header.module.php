<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<header>
    <div class="pull-left" style="margin-top:10px;">
        <i class="fa fa-bars fa-2x toggle-btn page-left chhotu-page-left" onclick="sidebar();" style="padding-left:10px;cursor: pointer;"></i> <span class="h3" style="padding-left:5px;">  <b><?php echo $this->lang['TITLE'] ?></b></span>
    </div>
    <div id="qwe" class="pull-right"></div>
    <div class="pull-right">
        <a href="" data-ipopup="user/change-password.module">Change Password</a>
    </div>
</header>
<script>
    function sidebar(){
        $(".nav-side-menu").toggleClass("chhotu");
        $(".page-container").toggleClass("chhotu-page-container");
        $(".page-left").toggleClass("chhotu-page-left");
        //$('.nav-side-menu [data-toggle="tooltip"]').tooltip("enable");
    }
</script>