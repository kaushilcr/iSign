<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
session_write_close();

$user_id = "new";
?>
<form action="<?php echo BASE_URL?>user/user.process?process-type=save" method="post" data-loader="">
    <input type="hidden" name="user_id" value="<?php echo @$user_id; ?>">
    <div class="form-group">
        <label for="first_name_id">Name</label>
        <div class="row">
            <div class="col-xs-6">             
                <input type="text" name="first-name" id="first_name_id" class="form-control input-sm" value="<?php echo @$user['first_name']?>" placeholder="First Name" required>
            </div>
            <div class="col-xs-6">
                <input type="text" name="last-name"  id="last_name_id" class="form-control input-sm" value="<?php echo @$user['last_name']; ?>" placeholder="Last Name">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="username">Username</label>       
                <input type="text" name="username" id="username" class="form-control input-sm" value="<?php echo @$user['username'] ?>" placeholder="Username" required>
            </div>
            <div class="col-xs-6">
                <label for="password">
                    Password
                    <i class="fa fa-eye password-eye pull-right"></i>
                </label>
                <input type="password" name="password"  id="password" class="form-control input-sm showpassword" placeholder="Password">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="roleId">Role</label>
        <select name="role" id="roleId" class="form-control input-sm">
            <?php foreach($roles as $role_id=>$role) { ?>
            <option value="<?php echo @$role_id ?>"<?php if($role_id == @$user['role_id']) echo "selected"; ?>><?php echo @$role?></option>
            <?php } ?> 
        </select>
    </div>

    <div class="form-group text-center">
        <button class="btn btn-primary btn-xs" type="submit">Save</button>
        <button class="btn btn-info btn-xs" type="reset">Reset</button>
        <button class="btn btn-default btn-xs" type="button" onclick='$("#client").refreshModule()'>Close</button>
    </div>
</form>
