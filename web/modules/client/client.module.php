<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close(); ?>
<?php
/* 3 States for staff_id

Not set  =  List 
"new" = New Form
"edit" = Edit Entry

*/

if(!isset($_REQUEST['client_id'])){
    $client_id ="";
    $this->loadModule("client/client-list");
    return;
}else{
    $client_id = $_REQUEST['client_id'];
    if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
        $this->loadModule("client/client-detail");
    else
        $this->loadModule("client/client-form");
    return;
}
?>