<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php session_write_close();?>
<?php 


?>
<button class="btn btn-danger btn-circle" onclick="newClient();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped" id="client-list" width="100%">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<div style="height:30px"></div>
<script>
    function newClient(){
        $("#client").refreshModule({
            data: {
                client_id: "new"
            }
        })
    }
    function editClient(client_id){
        $("#client").refreshModule({
            data: {
                client_id: client_id,
                type:"edit"
            }
        })
    }

    $('#client-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>client/client.process?process-type=list",
        "columnDefs": [ { orderable: false, targets: [3] } ],
        "language": {
            "lengthMenu": "_MENU_"
        }
    }).on("draw.dt",function(){
        $("form:not(.no-ajax)").ajaxsubmit2();
    });

</script>
