<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!isset($_REQUEST['page_id'])){
    $page_id ="new";
}else{
    $page_id = $_REQUEST['page_id'];
    $result = $this->db->query("select * from `_page` where `id` = $page_id");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "No records found";
        return;
    }
    $page = mysqli_fetch_array($result);
}

?>
<form action="<?php echo BASE_URL?>page.process" method="post" data-loader="" id="page-form">
    <input type="hidden" value="<?php echo $page_id?>" name="page_id">
    <div class="form-group">
        <label for="page-title">Name</label>
        <div class="row">
            <div class="col-xs-6">             
                <input type="text" name="title" id="page-title" class="form-control input-sm" placeholder="Title" value="<?php echo @$page['title']; ?>" required>
            </div>
            <div class="col-xs-6">
                <input type="text" name="name"  id="page-name" class="form-control input-sm" placeholder="Page name" value="<?php echo @$page['name'];?>" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="column_ratio">Column Ratio</label>       
                <input type="text" name="column_ratio" id="column_ratio" class="form-control input-sm" value="<?php echo @$page['view']?>" placeholder="Column Ratio (9:3)">
            </div>
            <div class="col-xs-6">
                <label for="page-icon">Icon (from Font-Awesome)</label>       
                <input type="text" name="icon" id="page-icon" class="form-control input-sm" value="<?php echo @$page['icon']?>" placeholder="class file of icon (Font-Awesome)">
            </div>
        </div>
    </div>    
    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">
            <?php echo ($page_id=="new")?"Add":"Update"; ?>
        </button>
        <button class="btn btn-primary" type="reset">
            Reset
        </button>
        <?php if ($page_id!="new"){?>
        <button class="btn btn-default" type="button" onclick='$("#new-page").refreshModule();'>
            Close
        </button>
        <?php } ?>
    </div>
</form>
<script>
    $("#page-title").change(function(){
        var text=$(this).val().toLowerCase().trim().replace(/\s/g, "-");
        $("#page-name").val(text);
    });
</script>