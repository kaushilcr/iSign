<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
//Field List with Entity Name
$result = $this->db->query("select f.`id`,f.`name` as field_name,e.`name` as entity_name from `field` f,`entity` e where e.`id`=f.`entity_id`");
if(!$result){
    echo "Error in executing script: ".$this->db->error();
    exit;
}
$field_list = array();
while($row=mysqli_fetch_array($result)){
    $field_list[]=$row;
}
if(!isset($_REQUEST['field_id'])){
    $field_id ="";

}else{
    $field_id = $_REQUEST['field_id'];
    //Entity List
    $result = $this->db->get("entity");
    if(!$result){
        echo "Error in executing script: ".$this->db->error();
        exit;
    }
    $entities = array();
    while($row=mysqli_fetch_array($result)){
        $entities[]=$row;
    }

    if(ctype_digit($field_id)){
        $result = $this->db->query("select * from `field` where `id` = $field_id");
        if(!$result) {
            echo "Error in executing script: ".$this->db->error();
            return;
        }
        if(mysqli_num_rows($result)==0){
            echo "No records found";
            return;
        }
        $field = mysqli_fetch_array($result);
    }
}
?>
<?php if($field_id==""){?>
<button class="btn btn-danger btn-circle" onclick="newField();" style="position:absolute;right:15px;bottom:15px;">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Under Entity</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($field_list as $field){?>
        <tr>
            <td><?php echo $field['id']?></td>
            <td><?php echo $field['field_name']?></td>
            <td><?php echo @$field['entity_name']?></td>
            <td>
                <button class="btn btn-warning btn-xxs" onclick="editfield(<?php echo $field['id']?>)"><i class="fa fa-pencil"></i></button>
                <form action="<?php echo BASE_URL?>remove-field.process" method="post" style="display:inline;">
                    <input type="hidden" value="<?php echo $field['id']?>" name="field_id">
                    <button class="btn btn-danger btn-xxs" type="submit" data-confirm="<?php echo $this->lang['CONFIRM_REMOVE']?>"><i class="fa fa-remove"></i></button>
                </form>
            </td>     
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php }else{?>
<form action="<?php echo BASE_URL?>field.process" method="post" data-loader="" id="page-form">
    <input type="hidden" value="<?php echo $field_id?>" name="field_id">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label for="field-name">Name</label>             
                <input type="text" name="name" id="field-name" class="form-control input-sm" placeholder="Field name" value="<?php echo @$field['name']; ?>" required>
            </div>
            <div class="col-xs-6">
                <label for="field-entity">Under</label>
                <select name="entity_id" id="field-entity" class="form-control input-sm">
                    <option value="">Please select</option>
                    <?php foreach($entities as $entity) { ?>
                    <option value="<?php echo $entity['id']?>" <?php if(@$field['entity_id']==$entity['id']) echo "selected"?>><?php echo $entity['name']?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label for="field-type">Type</label>             
                <select name="type" id="field-type" class="form-control input-sm">
                    <option value="" <?php if(@$field['type']=="") echo "selected"?>>Please select</option>
                    <option value="text" <?php if(@$field['type']=="text") echo "selected"?>>text</option>
                    <option value="number" <?php if(@$field['type']=="number") echo "selected"?>>number</option>
                    <option value="email" <?php if(@$field['type']=="email") echo "selected"?>>email</option>
                    <option value="select" <?php if(@$field['type']=="select") echo "selected"?>>select</option>
                    <option value="ajaxselect" <?php if(@$field['type']=="ajaxselect") echo "selected"?>>ajaxselect</option>
                </select>
            </div>
            <div class="col-xs-6">
                <label for="field-default">Default value</label>
                <input type="text" name="default" id="field-default" class="form-control input-sm" placeholder="Default value to be displayed" value="<?php echo @$field['default']; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label for="field-link">Linked with</label>             
                <select name="linked-with" id="field-link" class="form-control input-sm">
                    <option value="">None</option>
                    <?php foreach($field_list as $f) { ?>
                    <option value="<?php echo $f['id']?>" <?php if(@$field['linked_with']==$f['id']) echo "selected"?>><?php echo $f['field_name']." | ".$f['entity_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-xs-6">
                <label for="field-required">Required</label>
                <br>
                <input type="checkbox" name="required" id="field-required" <?php if(@$field['required']==1) echo "checked"?> class="" data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>
        </div>
    </div>

    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">
            <?php echo ($field_id=="new")?"Add":"Update"; ?>
        </button>
        <button class="btn btn-primary" type="reset">
            Reset
        </button>
        <button class="btn btn-default" type="button" onclick='$("#field").refreshModule();'>
            Close
        </button>
    </div>
</form>
<?php } ?>
<script>
    function newField(){
        $("#field").refreshModule({
            data: {
                field_id: "new"
            }
        })
    }
    function editfield(field_id){
        $("#field").refreshModule({
            data: {
                field_id: field_id
            }
        })
    }
    $("input[type=checkbox]:not('.no-switch')").bootstrapSwitch();
</script>