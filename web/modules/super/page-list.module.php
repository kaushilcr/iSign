<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$result =$this->db->query("SELECT * from `_page`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    echo "No Pages yet !";
}
$pages = array();
while($row = mysqli_fetch_array($result)){
    $row['icon'] = ($row['icon']=="")?"circle-thin":$row['icon'];
    $pages[] = $row;
}
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>File Name(for url)</th>
            <th class="text-center">Icon</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($pages as $page){?>
        <tr>
            <td><?php echo $page['id']?></td>
            <td><?php echo $page['title']?></td>
            <td><?php echo $page['name']?></td>
            <td class="text-center"><i class="fa fa-<?php echo $page['icon']?>"></i></td>
            <td>
                <button class="btn btn-warning btn-xxs" onclick="edit_page(<?php echo $page['id']?>)"><i class="fa fa-pencil"></i></button>
                <form action="<?php echo BASE_URL?>remove-page.process" method="post" style="display:inline;">
                    <input type="hidden" value="<?php echo $page['id']?>" name="page_id">
                    <button class="btn btn-danger btn-xxs" type="submit" data-confirm="<?php echo $this->lang['CONFIRM_REMOVE']?>"><i class="fa fa-remove"></i></button>
                </form>
            </td>        
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    function edit_page(page_id){
        $("#new-page").refreshModule({
            data: {
                page_id: page_id
            }
        })
    }
</script>