<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
//List of Pages
$result = $this->db->query("select `id`,`title` from `_page`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
$pages = array();
while($row = mysqli_fetch_array($result)){
    $pages[]=$row;
}

//List of Roles
$result = $this->db->query("select * from `_role`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
$roles = array();
while($row = mysqli_fetch_array($result)){
    $roles[]=$row;
}

?>
<form action="role-page-association.process" method="post">
    <div class="row">
        <div class="col-sm-6">
            <select name="role_id" id="" class="form-control input-sm">
                <?php foreach($roles as $role) {?>
                <option value="<?php echo $role['id']?>"><?php echo $role['role_name']?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-sm-6">
            <select name="page_id" id="" class="form-control input-sm">
                <?php foreach($pages as $page) {?>
                <option value="<?php echo $page['id']?>"><?php echo $page['title']?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12 text-center">
            <button class="btn-xs btn-primary" type="submit" name="link">Link</button>
            <button class="btn-xs btn-primary" type="submit" name="unlink">Unlink</button>
        </div>
    </div>
</form>