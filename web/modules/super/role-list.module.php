<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$result =$this->db->query("SELECT * from `_role`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    echo "No roles yet !";
}
$roles = array();
while($row = mysqli_fetch_array($result)){
    $roles[] = $row;
}
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Role Code</th>
            <th>Role Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($roles as $role){?>
        <tr>
            <td><?php echo $role['id']?></td>
            <td><?php echo $role['role_code']?></td>
            <td><?php echo $role['role_name']?></td>
            <td>
                <button class="btn btn-warning btn-xxs" onclick="edit_role(<?php echo $role['id']?>)"><i class="fa fa-pencil"></i></button>
                <form action="<?php echo BASE_URL?>remove-role.process" method="post" style="display:inline;">
                    <input type="hidden" value="<?php echo $role['id']?>" name="role_id">
                    <button class="btn btn-danger btn-xxs" type="submit" data-confirm="<?php echo $this->lang['CONFIRM_REMOVE']?>"><i class="fa fa-remove"></i></button>
                </form>
            </td>        
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    function edit_role(role_id){
        $("#role").refreshModule({
            data: {
                role_id: role_id
            }
        })
    }
</script>