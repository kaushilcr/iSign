<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!isset($_REQUEST['module_id'])){
    $module_id ="new";
    $module = array();
}
else{
    $module_id = $_REQUEST['module_id'];
    $result = $this->db->query("select * from `_module` where `id` = $module_id");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "No records found";
        return;
    }
    $module = mysqli_fetch_array($result);
}
//var_dump($module);

$result = $this->db->query("select `id`,`title` from `_page`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
$pages = array();
while($row = mysqli_fetch_array($result)){
    $pages[]=$row;
}
?>
<form action="<?php echo BASE_URL?>module.process" method="post" data-loader="" role="form" id="module-form">
    <input type="hidden" value="<?php echo $module_id?>" name="module_id">
    <div class="form-group">

        <div class="row">
            <div class="col-xs-6">    
                <label for="module-title">Title</label>         
                <input type="text" name="title" id="module-title" class="form-control input-sm" placeholder="Module Title" value="<?php echo @$module['title']?>" required data-data="<?php echo @$module['title'] ?>">
            </div>

            <div class="col-xs-6">
                <label for="page_id">Page</label>
                <select name="page_id" class="form-control input-sm" >
                    <?php foreach($pages as $page){?>
                    <option value="<?php echo $page['id']?>" <?php if($page['id']==@$module['page_id']) echo "selected"?> ><?php echo $page['title']?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="module-class">Module Class file (for color)</label>       
                <input type="text" name="module_class" id="module-class" class="form-control input-sm" value="<?php echo @$module['class']?>" placeholder="panel-default">
            </div>

            <div class="col-xs-6">
                <label for="module-name">Module File Name(for url)</label>
                <input type="text" name="module_name"  id="module-name" class="form-control input-sm" placeholder="Module name" value="<?php echo @$module['module_name']?>" required>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="module-process">Process File Name</label>       
                <input type="text" name="process_name" id="module-process" class="form-control input-sm" value="<?php echo @$module['process_name']?>" placeholder="">
            </div>

            <div class="col-xs-6">
                <label for="module-name">Template</label>
                <select name="module_template" class="form-control input-sm">
                    <option value="">Empty</option>
                    <option value="List with Form">List with Form</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-xs-3">      
                <label for="movable">Movable</label>       
                <input type="checkbox" name="movable" id="movable" class="" <?php if(!isset($module['movable']) || @$module['movable']) echo "checked"?> data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>

            <div class="col-xs-3">
                <label for="collapsible">Collapsible</label>
                <input type="checkbox" name="collapsible" id="collapsible" class="" <?php if(!isset($module['collapsible']) || @$module['collapsible']) echo "checked"?>  data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>
            <div class="col-xs-3">
                <label for="maximize">Maximize</label>
                <input type="checkbox" name="expandable" id="maximize" class="" <?php if(!isset($module['expandable']) || @$module['expandable']) echo "checked"?>  data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>
            <div class="col-xs-3">
                <label for="no-padding">No Padding</label>       
                <input type="checkbox" name="no_padding" id="no-padding" class="" <?php if(@$module['no_padding']) echo "checked"?>  data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>
        </div>
    </div>    
    <div class="form-group">
        <div class="row">
            <div class="col-xs-4">
                <label for="html-id">HTML ID</label>
                <input type="text" name="html_id" min="1" max="12" id="html-id" class="form-control input-sm" placeholder="Module name" value="<?php echo @$module['html_id'];?>" required>
            </div>
            <div class="col-xs-4">
                <label for="column-number">Column Number</label>
                <input type="number" name="column_number" min="1" max="12" id="column-number" class="form-control input-sm" placeholder="Module name" value="<?php if(@$module['column_number']) echo @$module['column_number']; else echo "1";?>" required>
            </div>
            <div class="col-xs-4">
                <label for="module-sort">Sort</label>
                <input type="text" name="sort"  id="module-sort" class="form-control input-sm" placeholder="Module name" value="<?php if(@$module['sort']) echo @$module['sort']; else echo "1";?>" required>
            </div>
        </div>
    </div>    

    <div class="form-group text-center">
        <button class="btn btn-success" type="submit" form="module-form">
            <?php echo ($module_id=="new")?"Add":"Update"; ?>
        </button>
        <button class="btn btn-primary" type="reset">
            Reset
        </button>
        <?php if ($module_id!="new"){?>
        <button class="btn btn-default" type="button" onclick='$("#new-module").refreshModule();'>
            Close
        </button>
        <?php } ?>
    </div>
</form>
<script>
    <?php if($module_id=="new"){?>
    $("#module-title").change(function(){
        var text=$(this).val().toLowerCase().trim().replace(/\s/g, "-");
        $("#module-name").val(text+".module");
    });
    <?php } ?>
    $("input[type=checkbox]:not('.no-switch')").bootstrapSwitch();
</script>