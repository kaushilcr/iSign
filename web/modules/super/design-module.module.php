<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!isset($_REQUEST['module_id'])){
    echo "Kindly choose Module to design";
    return;
}
else{
    $module_id = $_REQUEST['module_id'];
    $result = $this->db->query("select * from `module` where `id` = $module_id");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "No records found";
        return;
    }
    $module = mysqli_fetch_array($result);
    //List of Entity
    $result = $this->db->get("entity");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    $entities = array();
    while($row = mysqli_fetch_array($result)){
        $entities[] = $row;
    }
}
?>
<h4 class="text-center"><?php echo $module['title']?> <small>(<?php echo $module['module_name']?>)</small></h4>
<hr>

<div id="design-area"></div>
<div id="save-design-area" style="position:absolute;top:-1000px;visibility:hidden;"></div>

<hr>
<div class="row">
    <div class="col-sm-2">
        <input type="text" class="form-control input-sm" placeholder="Column Ratio" id="ratio" value="4:4:4">
    </div>
    <div class="col-sm-1">
        <button class="btn btn-circle btn-info" onclick="addRow();"><i class="fa fa-plus"></i></button>
    </div>
    <div class="col-sm-2">
        <select name="type" id="new-field" class="form-control input-sm">
            <option value="">Select Field...</option>
            <option value="text">text</option>
            <option value="email">email</option>
            <option value="password">password</option>
            <option value="checkbox">checkbox</option>
            <option value="select">select</option>
            <option value="textarea">textarea</option>
            <option value="submit">Submit Button</option>
            <option value="reset">Reset Button</option>
            <option value="button">Normal Button</option>
        </select>
        <label for="">Label Text</label>
        <input type="text" class="form-control input-sm" id="labelText">
        <label for="">Placeholder</label>
        <input type="text" class="form-control input-sm" id="placeHolderText">
    </div>
    <div class="col-sm-2">
        <select name="entity" id="" class="form-control input-sm">
            <option value="">Select Entity...</option>
            <?php foreach($entities as $e) {?>
            <option value="<?php echo $e['id']?>"><?php echo $e['name']?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-sm-4">
        <div class="input-place-holder generated-field">
            <div>
                <input type="text" class="form-control input-sm">
                <div class="btn btn-circle btn-info drag-element not-in-design"><i class="fa fa-arrows"></i></div>
            </div>
        </div>
    </div>
    <div class="col-sm-1">
        <button class="btn btn-danger btn-sm pull-right" onclick="saveForm()" id="saveFormBtn">SAVE</button>
    </div>

</div>
<hr>
<!--<textarea name="" id="jsonData" cols="30" rows="10" class="form-control input-xs"></textarea>-->

<script>

    jQuery.fn.swap = function(b) {
        b = jQuery(b)[0];
        var a = this[0],
            a2 = a.cloneNode(true),
            b2 = b.cloneNode(true),
            stack = this;

        a.parentNode.replaceChild(b2, a);
        b.parentNode.replaceChild(a2, b);

        stack[0] = a2;
        return this.pushStack( stack );
    };
    window.rowId=0;

    function validateRatio(){
        var ratio = $("#ratio").val().split(":");
        for (i = 0; i < ratio.length; ++i) {
            ratio[i]=Number(ratio[i]);
            if(isNaN(ratio[i]) || ratio[i]===0){
                alert("Kindly provide ratio in correct format");
                return false;
            }
            if(ratio[i]>12){
                alert("Column cannot be greater than 12");
                return false;
            }
        }
        return true;
    }
    function addRow(){
        if(!validateRatio()) return;
        var ratio = $("#ratio").val().split(":");
        var html="";
        rowId++;

        html+='<div class="row in-design rowID'+rowId+'">';

        for (i = 0; i < ratio.length; ++i) {
            ratio[i]=Number(ratio[i]);
            html+='<div class="col-sm-'+ratio[i]+' input-place-holder"></div>';
        }
        html+='</div>';
        $("#design-area").append(html);
        addControl(rowId);
        attachMove();
    }
    function removeRow(rId){
        $(rId).remove();
    }
    function moveRowDown(rId){
        //console.log($(rId).next().next().attr("class"));
        var cur = $(rId);
        var curCtrl = cur.next();
        var nxt = curCtrl.next();
        var nxtCtrl = nxt.next();

        cur.swap(nxt);
        curCtrl.swap(nxtCtrl);   
        attachMove();

    }

    function moveRowUp(rId){
        //console.log($(rId).prev().prev().attr("class"));
        var cur = $(rId);
        var curCtrl = cur.next();
        var prevCtrl = cur.prev();
        var prev =prevCtrl.prev();
        cur.swap(prev);
        curCtrl.swap(prevCtrl);   
        attachMove();
    }
    function addControl(rId){
        $("#design-area").append('<div class="row not-in-design rowID'+rId+'"><div class="col-sm-12 text-right"><button class="btn btn-warning btn-xxs" onclick=\'moveRowUp(".rowID'+rId+'")\'><i class="fa fa-angle-up"></i></button>'+
                                 '<button class="btn btn-warning btn-xxs" onclick=\'moveRowDown(".rowID'+rId+'")\'><i class="fa fa-angle-down"></i></button>'+
                                 '<button class="btn btn-danger btn-xxs" onclick=\'removeRow(".rowID'+rId+'")\'><i class="fa fa-remove"></i></button></div></div>');
    }
    function attachMove(){
        $(".input-place-holder").sortable({
            handle: '.drag-element',
            cursor: "move",
            opacity: 0.5,
            connectWith: '.input-place-holder',
            placeholder: "ui-sortable-placeholder"
        });
    }
    function saveForm(){
        $("#saveFormBtn").attr("disabled","disabled");

        var data=$("#design-area").html();
        $("#save-design-area").html(data);
        $("#save-design-area .not-in-design").remove();
        var data="<div>"+$("#save-design-area").html()+"</div>";


        $.post( "<?php echo BASE_URL?>save-module.process", { form: data },function(result){
            console.log(result);
            $("#saveFormBtn").removeAttr("disabled");
        });
    }
    $("#new-field, #labelText, #placeHolderText").change(function(){
        var fieldType = $("#new-field").val();
        var labelText = $("#labelText").val();
        var placeHolderText = $("#placeHolderText").val();
        var field = "";
        switch(fieldType){
            case 'text':
                if(labelText!="") labelText = '<label>'+labelText+'</label>';
                field = labelText+'<input type="text" class="form-control input-sm" placeholder="'+placeHolderText+'">';
                break;
            case 'email':
                if(labelText!="") labelText = '<label>'+labelText+'</label>';
                field = labelText+'<input type="email" class="form-control input-sm" placeholder="'+placeHolderText+'">';
                break;
            case 'password':
                if(labelText!="") labelText = '<label>'+labelText+'</label>';
                field = labelText+'<input type="password" class="form-control input-sm" placeholder="'+placeHolderText+'">';
                break;
            case 'select':
                if(labelText!="") labelText = '<label>'+labelText+'</label>';
                var option="";
                if(placeHoldertext!="") option ='<option value="">'+placeHoldertext+'</option>';
                field = labelText+'<select class="form-control input-sm"></select>';
                break;
            case 'checkbox':
                field = '<input type="checkbox" data-size="mini" data-on-text="Yes" data-off-text="No">';
                if(labelText!="") field = '<label>'+field+labelText+'</label>';
                break;
            case 'submit':
                field = '<button class="btn btn-success btn-sm" type="submit">'+labelText+'</button>';
                break;
            case 'reset':
                field = '<button class="btn btn-primary btn-sm" type="reset">'+labelText+'</button>';
                break;
            case 'button':
                field = '<button class="btn btn-info btn-sm">'+labelText+'</button>';
                break;
            case 'textarea':
                if(labelText!="") labelText = '<label>'+labelText+'</label>';
                field = labelText+'<textarea class="form-control input-sm" placeholder="'+placeHolderText+'"></textarea>';
                break;
            default:
                return;
        }
        $(".generated-field").html('<div>'+field+'<div class="btn btn-circle btn-info drag-element not-in-design"><i class="fa fa-arrows"></i></div></div>');
    });
</script>
<style>
    #design-area .row.in-design{
        background:#ccc;
        margin-bottom:10px;
    }
    #design-area .row.in-design > div{
        min-height:40px;
        border:2px dashed #585858;
    }
    #design-area .row.not-in-design > div{
        min-height:20px;
    }
    #design-area .row.not-in-design > div > .btn{
        margin-top:-20px;
    }
</style>