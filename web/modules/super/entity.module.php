<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!isset($_REQUEST['entity_id'])){
    $entity_id ="";
}else{
    $entity_id = $_REQUEST['entity_id'];
    if(ctype_digit($entity_id)){
        $result = $this->db->query("select * from `entity` where `id` = $entity_id");
        if(!$result) {
            echo "Error in executing script: ".$this->db->error();
            return;
        }
        if(mysqli_num_rows($result)==0){
            echo "No records found";
            return;
        }
        $entity = mysqli_fetch_array($result);
    }
}
?>
<?php if($entity_id==""){
    //List Entities
    $result =$this->db->query("SELECT * from `entity`");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    /*    if(mysqli_num_rows($result)==0){
        echo '<p class="text-center">No Entities yet !</p>';
    }*/
    $entities = array();
    while($row = mysqli_fetch_array($result)){
        $entities[] = $row;
    }
?>
<button class="btn btn-danger btn-circle" onclick="newEntity();" style="position:absolute;right:15px;bottom:15px;">
    <i class="fa fa-plus"></i>
</button>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($entities as $entity){?>
        <tr>
            <td><?php echo $entity['id']?></td>
            <td><?php echo $entity['name']?></td>
            <td><?php echo $entity['description']?></td>
            <td>
                <button class="btn btn-warning btn-xxs" onclick="editEntity(<?php echo $entity['id']?>)"><i class="fa fa-pencil"></i></button>
                <form action="<?php echo BASE_URL?>remove-entity.process" method="post" style="display:inline;">
                    <input type="hidden" value="<?php echo $entity['id']?>" name="entity_id">
                    <button class="btn btn-danger btn-xxs" type="submit" data-confirm="<?php echo $this->lang['CONFIRM_REMOVE']?>"><i class="fa fa-remove"></i></button>
                </form>
            </td>     
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php }else{?>
<form action="<?php echo BASE_URL?>entity.process" method="post" data-loader="" id="page-form">
    <input type="hidden" value="<?php echo $entity_id?>" name="entity_id">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label for="entity-name">Name</label>             
                <input type="text" name="name" id="entity-name" class="form-control input-sm" placeholder="Entity name" value="<?php echo @$entity['name']; ?>" required>
            </div>
            <div class="col-xs-6">
                <label for="entity-description">Description</label>
                <textarea name="description" id="" rows="2" class="form-control input-sm"><?php echo @$entity['description']; ?></textarea>
            </div>
        </div>
    </div>

    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">
            <?php echo ($entity_id=="new")?"Add":"Update"; ?>
        </button>
        <button class="btn btn-primary" type="reset">
            Reset
        </button>
        <button class="btn btn-default" type="button" onclick='$("#entity").refreshModule();'>
            Close
        </button>
    </div>
</form>
<?php } ?>
<script>
    function newEntity(){
        $("#entity").refreshModule({
            data: {
                entity_id: "new"
            }
        })
    }
    function editEntity(entity_id){
        $("#entity").refreshModule({
            data: {
                entity_id: entity_id
            }
        })
    }
</script>