<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(isset($_REQUEST['referrer'])) $referrer = $_REQUEST['referrer'];
else $referrer="";
$result =$this->db->query("SELECT * from `_module`");
if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    echo "No Pages yet !";
}
$modules = array();
while($row = mysqli_fetch_array($result)){
    $modules[] = $row;
}
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>File Name(for url)</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($modules as $module){?>
        <tr>
            <td><?php echo $module['id']?></td>
            <td><?php echo $module['title']?></td>
            <td><?php echo $module['module_name']?></td>
            <td>
                <?php if($referrer=="entity"){ ?>
                <button class="btn btn-warning btn-xxs" onclick="design_module(<?php echo $module['id']?>)"><i class="fa fa-pencil-square-o"></i></button>
                <?php }else{ ?>
                <button class="btn btn-warning btn-xxs" onclick="edit_module(<?php echo $module['id']?>)"><i class="fa fa-pencil"></i></button>
                <form action="<?php echo BASE_URL?>remove-module.process" method="post" style="display:inline;">
                    <input type="hidden" value="<?php echo $module['id']?>" name="module_id">
                    <button class="btn btn-danger btn-xxs" type="submit" data-confirm="<?php echo $this->lang['CONFIRM_REMOVE']?>"><i class="fa fa-remove"></i></button>
                </form>
                <?php } ?>
            </td>     
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    function design_module(module_id){
        $("#design-module").refreshModule({
            data: {
                module_id: module_id
            }
        })
    } 
    function edit_module(module_id){
        $("#new-module").refreshModule({
            data: {
                module_id: module_id
            }
        })
    }
</script>