<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!isset($_REQUEST['role_id'])){
    $role_id ="new";
}else{
    $role_id = $_REQUEST['role_id'];
    $result = $this->db->query("select * from `_role` where `id` = $role_id");
    if(!$result) {
        echo "Error in executing script: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "No records found";
        return;
    }
    $role = mysqli_fetch_array($result);
}

?>
<form action="<?php echo BASE_URL?>role.process" method="post" data-loader="" id="role-form">
    <input type="hidden" value="<?php echo $role_id?>" name="role_id">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label for="role-name">Name</label>
                <input type="text" name="name" id="role-name" class="form-control input-sm" placeholder="Role Name" value="<?php echo @$role['role_name']; ?>" required>
            </div>
            <div class="col-xs-6">
                <label for="role-code">Role Code</label>&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-original-title="Unique, Only AlphaNumeric, All Small"></i>
                <input type="text" name="code"  id="role-code" class="form-control input-sm" placeholder="Role Code" value="<?php echo @$role['role_code'];?>" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">      
                <label for="enabled">Enabled</label>       
                <input type="checkbox" name="enabled" id="enabled" class="" <?php if(!isset($role['enabled']) || @$role['enabled']) echo "checked"?> data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>

            <div class="col-xs-6">
                <label for="visible">Visible</label>
                <input type="checkbox" name="visible" id="visible" class="" <?php if(!isset($role['visible']) || @$role['visible']) echo "checked"?>  data-size="mini" data-on-text="Yes" data-off-text="No">
            </div>

        </div>
    </div>    
    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">
            <?php echo ($role_id=="new")?"Add":"Update"; ?>
        </button>
        <button class="btn btn-primary" type="reset">
            Reset
        </button>
        <?php if ($role_id!="new"){?>
        <button class="btn btn-default" type="button" onclick='$("#role").refreshModule();'>
            Close
        </button>
        <?php } ?>
    </div>
</form>
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $("#role-name").change(function(){
        var text=$(this).val().toLowerCase().trim().replace(/\s/g, "");
        $("#role-code").val(text);
    });
    $("input[type=checkbox]:not('.no-switch')").bootstrapSwitch();
</script>