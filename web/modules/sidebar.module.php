<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if($this->session->getData("PROFILE_PIC"))
    $profile = BASE_URL."user-img/".$this->session->getData("PROFILE_PIC").".png";
else
    $profile = BASE_URL."user-img/default.png";
$role_code = $this->session->getData("ROLE_CODE");
$result = $this->db->query("select p.* from `_role_page` rp,`_role` r,`_page` p where r.`id`=rp.`role_id` AND p.`id`=rp.`page_id` AND r.`role_code`='$role_code' order by rp.`sort`");
if(!$result){
    echo "Not able to load List : ".$this->db->error();
    return;
}
$pages = array();
while($row = mysqli_fetch_array($result)){
    $pages[] = $row;
}
?>
<div class="nav-side-menu chhotu">
    <div class="user-details">
        <img src="<?php echo $profile?>" alt="" class="profile-pic img-thumbnail img-circle">
        <div class="details">
            <div class="user-name"><?php echo $this->session->getData("FIRST_NAME")." ".$this->session->getData("LAST_NAME");?></div>
            <div class="role-name"><?php echo $this->session->getData("ROLE");?></div>
        </div>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    <div class="menu-list">
        <ul id="menu-content" class="menu-content collapse out ">
            <?php foreach($pages as $page){?>
            <li class="<?php if(PAGE==$page['name'])echo "active"?>"data-container="body" data-toggle="tooltip" data-placement="right" title="<?php echo $page['title']?>">
                <a href="<?php echo BASE_URL ?><?php echo $page['name']?>.html">
                    <i class="fa fa-<?php $icon = ($page['icon']=="")? "circle-thin":$page['icon']; echo $icon;  ?> fa-lg"></i> <span class=""><?php echo $page['title']?></span>
                </a>
            </li>
            <?php }?>
            <li>
                <a href="<?php echo BASE_URL ?>logout.process" class="no-ajax">
                    <i class="fa fa-sign-out fa-lg"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="additional-info">
        Last Login: <?php echo $this->session->getData("LAST_LOGIN");?>
    </div>
</div>
<script>
    function loadPage(page_url,$li,page_title)
    {
        $.ajax({
            url:page_url,
            method: "get",
            success: function(response){
                //console.log(response);
                $(".nav-side-menu li").removeClass("active");
                $li.addClass("active");
                $(".page-container").html(response);
                afterPageLoad();
                history.pushState(response, "<?php echo TITLE?> : "+page_title, page_url);
            }
        });
    }
    $(".nav-side-menu a:not('.no-ajax')").click(function(e){
        //alert();
        page_url = $(this).attr("href");

        $li = $(this).parent("li");
        page_title = $li.attr("title");
        //alert(page_url);
        loadPage(page_url,$li,page_title);

        e.preventDefault();
    });
    $(window).on("popstate", function(e) {
        //alert(e);
        //console.log(e.originalEvent.state);
        //$li = $(".nav-side-menu li:first");
        //loadPage(e.originalEvent.state,$li,"");
        $(".page-container").html(e.originalEvent.state);
        afterPageLoad();
    });
    $('.nav-side-menu [data-toggle="tooltip"]').tooltip();
    //$('.nav-side-menu [data-toggle="tooltip"]').tooltip("disable");
</script>