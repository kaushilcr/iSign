<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!defined ('ACTIONURL')) define ('ACTIONURL',BASE_URL."login.process");
?>
<div class="center-box">
    <h2 class="text-center" >  <img src="<?php echo BASE_URL?>/assets/img/logo.gif" style="width:180px;"></h2>

    <div class="servermessage">
        <?php if (defined('AUTHMESSAGE')) {  ?>
        <div class="alert alert-info"><?php echo AUTHMESSAGE ?></div>
        <?php } ?>
    </div>
    <form class="form form-horizontal" role="form" action="<?php echo ACTIONURL;?>" method="post">
        <input type="hidden" value="login" name="action"/>
        <div class="input-group" style="border-radius:5px;">
            <input type="text" name="username" class="username form-control" placeholder="Username" id="username" >
            <div class="input-group-addon"><i class="fa fa-user"></i></div>
        </div>
        <div class="input-group" style="border-radius:5px; margin-top:15px;">
            <input type="password" class="form-control " id="password" placeholder="Password"  name="password" required >  <div class="input-group-addon"><i class="fa fa-key"></i></div>
        </div>
        <div class="text-center">
            <br>
            <button type="submit" class="login_button" id="enable_dissable" style="border-radius:5px;">Sign In </button>
        </div>
    </form>

</div>
<div class="footer text-center" style="text-shadow: 0px 0px 5px rgba(255,255,255, 1);" >
    <b>Powered by <a href="http://www.isign.co.in/" target="_blank">iSign Tech Solutions</a></b>
</div>
<style>
    .center-box{
        position: absolute;
        top: calc(48% - 150px);
        left: calc(50% - 175px);
        height: 300px;
        width: 350px;
        padding: 10px;
        z-index: 20;
    }
    .footer{
        position:absolute;
        bottom: 15%;
        left: calc(50% - 150px);
        width: 300px;
        z-index: 21;
        color: #000;
    }
    .footer a{
        color:#000 ;
        text-decoration:none;
    }

</style>
<?php
/*
<link rel="stylesheet" href="<?php echo BASE_URL ?>assets/css/supersized.css">

<script src="<?php echo BASE_URL ?>assets/js/supersized.3.2.7.min.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL ?>assets/js/supersized-init.js" type="text/javascript"></script>
*/    
?>
