<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
if(!$this->session->isLoggedIn()){
    $this->loadPage("login");
    return;
}
$role_code = $this->session->getData("ROLE_CODE");
$result  = $this->db->query("select p.* from `_role_page` rp, `_role` r, `_page` p
    WHERE rp.`role_id` = r.`id` AND
          rp.`page_id` = p.`id` AND
          r.`role_code` = '$role_code'
    ORDER BY
          p.`sort`
    LIMIT 1
");

if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    $this->loadModule("sidebar");
    $this->loadModule("header");

    return;
}
$row = mysqli_fetch_array($result);

?>
<script>
    window.location.href = "<?php echo BASE_URL ?><?php echo $row['name']?>.html";
</script>
<?php
    return;
?>
