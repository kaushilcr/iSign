<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>

<?php $this->loadModule("sidebar");?>
<?php $this->loadModule("header");?>
<div class="container-fluid page-container">
    <h3><?php echo $this->lang['404_ERROR']?></h3>
</div>
