<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>

<?php $this->loadModule("sidebar");?>
<?php $this->loadModule("header");?>
<div class="container-fluid page-container">
    <h3><?php echo $this->lang['ACCESS_DENIED_TEXT']?></h3>
</div>
