<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php if(!$this->session->isLoggedIn()){
    $this->loadPage("login");
    return;
}?>
<?php 
if(!defined("PAGE")){
    echo "PAGE not set";
    return;
}
$page=PAGE;
$result = $this->db->query("select * from `_page` where name='$page'");

if(!$result) {
    echo "Error in executing script : ".$this->db->error();
    return;
}

if(mysqli_num_rows($result)==0) {
    $this->loadPage($page); //It's not from DB, hence load it from file Disk
    return;
}
$row = mysqli_fetch_array($result);
$view = explode(":",$row['view']);
if(end($view)==0) array_pop ($view);
$total_column = count($view);
$no_right_padding_required = ($total_column==1) ? false : true;
$page_id = $row['id'];
$is_single_module = $row['is_single_module'];
//Check Authentication
$role_code = $this->session->getData("ROLE_CODE");

$result = $this->db->query("select * from `_role_page` rp, `_role` r
        WHERE
                rp.`role_id` = r.`id` AND
                r.`role_code` = '$role_code' AND
                rp.`page_id` = $page_id");

if(!$result) {
    echo "Error in executing script: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    $this->loadPage("access-denied");
    return;
}

?>
<?php
if(!(defined('AJAX_CALL') && AJAX_CALL)) {
    $this->loadModule("sidebar");
    $this->loadModule("header");
}
?>

<?php if(!(defined('AJAX_CALL') && AJAX_CALL)) { ?>
<div class="page-container container-fluid chhotu-page-container">
    <?php } ?>
    <div class="row">
        <?php $i=0;foreach($view as $view_){$i++;?>
        <?php
                                            //Modules
                                            $result = $this->db->query("select * from `_module` where `page_id`=$page_id and `column_number` = $i order by `sort`");
                                            $modules = array();while($row = mysqli_fetch_array($result)){$modules[] = $row;}

        ?>
        <div class="col-md-<?php echo $view_?> movable" <?php if($no_right_padding_required && $total_column!=$i){?> style="padding-right:0px" <?php } ?> >
            <?php foreach($modules as $module){?>
            <div class="panel <?php if(@$module['extended']) echo "extended"?> <?php echo $module['class']?>" data-href="<?php echo BASE_URL?><?php echo $module['module_name']?>" id="<?php echo $module['html_id']?>">
                <?php if($module['no_header']==0){?>
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $module['title']?>
                        <?php if($module['collapsible']) {?>                       
                        <div class="panel-btn minmax pull-right" data-toggle="collapse" data-target="#<?php echo $module['html_id']?> .panel-body">
                            <i class="fa minimize"></i>
                        </div>
                        <?php } ?>
                        <?php if($module['expandable']) {?>                       
                        <div class="panel-btn expand pull-right">
                            <i class="fa fa-expand"></i>
                        </div>
                        <?php } ?>
                        <?php if($module['movable']) {?>                       
                        <div class="panel-btn drag pull-right">
                            <i class="fa fa-arrows-alt"></i>
                        </div>
                        <?php } ?>
                        <div class="reload panel-btn pull-right">
                            <i class="fa fa-refresh"></i>
                        </div>
                        <div class="print panel-btn pull-right" style="display:none;">
                            <i class="fa fa-print"></i>
                        </div>
                        <div class="loading panel-btn pull-right" style="display:none">
                            <i class="fa fa-spinner fa-spin"></i>
                        </div>
                    </h3>
                </div>
                <?php } ?>
                <div class="panel-body no-padding collapse in">
                    <div <?php if(!$module['no_padding']){?>style="padding:15px;" <?php } ?>>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <?php }?>
    </div>
    <?php if(!(defined('AJAX_CALL') && AJAX_CALL)) { ?>
</div>
<?php } ?>
