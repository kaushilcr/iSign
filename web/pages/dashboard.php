<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php if(!$this->authenticateFirst("ADMIN")) return;?>
<?php $this->loadModule("sidebar");?>
<?php $this->loadModule("header");?>
<?php 

?>
<div class="page-container container-fluid">
    <div class="row">
        <div class="col-md-12 movable">
            <div class="panel panel-primary" data-href="<?php echo BASE_URL?>dashboard/report.module" id="report">
                <div class="panel-heading">
                    <h3 class="panel-title">Send Message
                        <div class="panel-btn minmax pull-right" data-toggle="collapse" data-target="#report .panel-body">
                            <i class="fa minimize"></i>
                        </div>
                        <div class="panel-btn expand pull-right">
                            <i class="fa fa-expand"></i>
                        </div>
                        <div class="panel-btn drag pull-right">
                            <i class="fa fa-arrows-alt"></i>
                        </div>
                        <div class="loading panel-btn pull-right" style="display:none">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                    </h3>
                </div>
                <div class="panel-body collapse in">

                </div>
            </div>
        </div>
    </div>
</div>
