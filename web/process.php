<?php
require_once 'config/config.php';
require_once CONFIG."db.config.php";
@include_once CONFIG."custom.config.php";
require_once CONFIG."general.config.php";
if(DB_REQUIRED)
    require_once CLASSES."db.class.php";

require_once CLASSES."init.class.php";


$init = new init();
$json = $init->loadClass("json");
if(defined("MAINTENANCE_MODE") && MAINTENANCE_MODE){
    $json->sendJSON("System is in Maintenance Mode");   
}


if(isset($_GET['action']))
    define ('ACTION',strtolower($_GET['action']));
else
    define ('ACTION','null');

if(isset($_GET['folder']))
    $folder = $_GET['folder']."/";
else
    $folder = "";

//Check for Authentication here.
if(ACTION!="login" && ACTION!="logout"){
    $role_code = $init->session->getData("ROLE_CODE");
    $sql ="SELECT m.* from `_module` m,`_page` p,`_role` r,`_role_page` rp
    WHERE 
        rp.`role_id` = r.`id` AND
        rp.`page_id` = p.`id` AND
        m.`page_id` = m.`page_id` AND
        r.`role_code` = '$role_code' AND
        m.`process_name` = '$folder".ACTION.".process'
";
    $result = $init->db->query($sql);
    //echo $sql;
    if(MULTILINGUAL) {
        if(file_exists($init->getLanguagePath().PROCESS.ACTION.".php")){
            $init->loadLanguage($init->getLanguagePath().PROCESS.ACTION.".php");
        }else{
            if(DEBUG) echo "Language not loaded !";
        }
    }

    if(!$result) $json->sendJSON("Error in excuting script: ".$init->db->error());
    if(mysqli_num_rows($result)==0) $json->sendJSON( $init->lang['ACCESS_DENIED_TEXT']); 
}

/*if(DB_REQUIRED){
    foreach($_REQUEST as $k=>$v){
        if(is_string($v))
            $_REQUEST[$k]=$init->db->escape($v);
        else if(is_array($v)){
            foreach($v as $vk=>$vv){
                if(is_string($v))
                    $v[$vk]=$init->db->escape($vv);
            }
            $_REQUEST[$k] = $v;
        }
    }
}*/

$init->loadProcess($folder.ACTION);
$json = $init->loadClass("json");
$json->sendJSON("No Condition matched !");
?>