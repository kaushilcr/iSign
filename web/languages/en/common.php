<?php if( ! defined('BASE_URL')) exit('No direct script access allowed'); ?>
<?php 
/* 
    Common text used in all the pages.
*/
?>
<?php 
    $this->lang['TITLE']='iSign Tech Solutions';
    $this->lang['TITLE_TEXT']='Title';

    $this->lang['MESSAGE_TEXT']='Message';
    $this->lang['IMAGE_TEXT']='Image';

    $this->lang['ACCESS_DENIED_TEXT'] = 'You do not have permission to access this page/module';
?>