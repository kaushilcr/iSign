<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
//List Entities
?>

<button class="btn btn-danger btn-circle" onclick="newClient();" style="position:absolute;right:15px;bottom:15px;" data-toggle="tooltip" data-placement="left" data-original-title="Add New Roll(s)">
    <i class="fa fa-plus"></i>
</button>
<table class="table data-table compact" id="client-list">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
<div style="height:30px"></div>




<script>
    function newClient(){
        $("#client").refreshModule({
            data: {
                client_id: "new"
            }
        })
    }
    function viewClient(client_id){
        $("#client").refreshModule({
            data: {
                client_id: client_id,
                type:"view"
            }
        })
    }
    function editClient(client_id){
        $("#client").refreshModule({
            data: {
                client_id: client_id,
                type:"edit"
            }
        })
    }
    $('#client-list').dataTable( {
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo BASE_URL ?>client-list.process",
        "columnDefs": [ { orderable: false, targets: [2] } ],
        "language": {
            "lengthMenu": "_MENU_",
            "info": "Showing _PAGE_ of _PAGES_"
        }
         }).on("draw.dt",function(){
        $("form:not(.no-ajax)").ajaxsubmit2();
    });
</script>