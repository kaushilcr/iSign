<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
/*
Replace %module% with your entiry name
*/
if(!isset($_REQUEST['%module%_id'])){
    $%module%_id ="";
    $this->loadModule("%module%/%module%-list");
    return;
}else{
    $%module%_id = $_REQUEST['%module%_id'];
    if(isset($_REQUEST['type']) && $_REQUEST['type']=="view")
        $this->loadModule("%module%/%module%-detail");
    else
        $this->loadModule("%module%/%module%-form");
    return;
}
?>