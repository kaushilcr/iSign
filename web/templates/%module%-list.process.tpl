<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$json=$this->loadClass("json");
$innerJson=$this->loadClass("json");

$count = $this->db->getCount("client");
$json->add("recordsTotal",$count);

$json->add("draw",@$_REQUEST['draw']+0);


$start = $_REQUEST['start'];
$length = $_REQUEST['length'];
$search = $_REQUEST['search'];

$searchTerm = $search['value'];
$where = "WHERE 1=1 "; 
if(!empty($searchTerm)){
    $where.= "AND (";
    $where.= "`name` like '%$searchTerm%' or `address` like '%$searchTerm%' or `vat_no`='$searchTerm' or `ecc_no`='$searchTerm' or `lbt_no`='$searchTerm'";
    $where.=") ";
}
$result = $this->db->query("select * from `client` $where LIMIT $start, $length");

if(!$result){
    $json->add("error","Error while Executing query: ".$this->db->error());
    $json->echoJSON();
}
$totalFiltered = mysqli_num_rows($result);

$json->add("recordsFiltered",$totalFiltered);
$i=0;
while($row = mysqli_fetch_array($result)){

    $data = array();
    $data[] = $row['id'];
    $data[] = $row['name'];
    $data[] = '<button class="btn btn-default btn-xxs" onclick="viewClient('.$row['id'].')"><i class="fa fa-eye"></i></button>'."&nbsp;".'<button class="btn btn-default btn-xxs" onclick="editClient('.$row['id'].')"><i class="fa fa-pencil"></i></button>';

    $innerJson->add($i++,$data);
}


/*

for($i=0;$i<10;$i++){
    $data = array();
    $data[] = $i;
    $data[] = $i*2;
    $data[] = $i*3;
    $data[] = '<button class="btn btn-default btn-xxs"> Some Action</button>';

}
*/
$json->add("data",$innerJson->getArray());

$json->echoJSON();

?>