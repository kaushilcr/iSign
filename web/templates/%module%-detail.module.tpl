<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
/*
replace %module%  with entityname
*/
$%module%_id = $_REQUEST['%module%_id'];

$result = $this->db->query("select * from `%module%` where `id`='$%module%_id'");
if(!$result){
    echo "Error in fetching %module% details: ".$this->db->error();
    return;
}
if(mysqli_num_rows($result)==0){
    echo "Invalid %module% ID";
    return;
}
$%module% = mysqli_fetch_array($result);

?>
<table class="table table-striped table-hover table-condensed printable">
    <tbody>
        <tr>
            <td>Name</td>
            <td><?php echo $%module%['name'] ?></td>
        </tr>
           <tr>
            <td>Short Name</td>
            <td><?php echo $%module%['short_code'] ?></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><?php echo $%module%['address'] ?></td>
        </tr>
        <tr>
            <td>VAT Number</td>
            <td><?php echo $%module%['vat_no'] ?></td>
        </tr>
        <tr>
            <td>ECC Number</td>
            <td><?php echo $%module%['ecc_no'] ?></td>
        </tr>
        <tr>
            <td>LBT Number</td>
            <td><?php echo $%module%['lbt_no'] ?></td>
        </tr>
        <tr>
            <td>Contact Person</td>
            <td><?php echo $%module%['contact_person'] ?></td>
        </tr>
        <tr>
            <td>Contact No</td>
            <td><?php echo $%module%['contact_no'] ?></td>
        </tr>
    </tbody>
</table>
<div class="text-center">
    <button class="btn btn-default btn-xs" type="button" onclick='$("#%module%").refreshModule();'>Close</button>
</div>
<script>
    $("#%module%").enablePrint();
</script>