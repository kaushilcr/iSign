<?php if( ! defined('BASE_URL')) exit('No direct script access allowed');?>
<?php
$client_id = $_REQUEST['client_id'];

if(ctype_digit($client_id)){
    $result = $this->db->query("select * from `client` where `id`='$client_id'");
    if(!$result){
        echo "Error in fetching vendor details: ".$this->db->error();
        return;
    }
    if(mysqli_num_rows($result)==0){
        echo "Invalid client ID";
        return;
    }
    $client = mysqli_fetch_array($result);
}
else
    $client_id = "new";
?>
<form action="<?php echo BASE_URL?>client.process">
    <input type="hidden" value="<?php echo $client_id?>" name="client_id">
    <input type="hidden" value="<?php echo @$_GET['popup']?>" name="popup">
    <div class="row">

        <div class="col-sm-12">
            <div class="form-group">
                <label for="client_name">Name</label>
                <input type="text" name="name" id="client_name" class="form-control input-xs" value="<?php echo @$client['name']?>">
            </div>
        </div>
         <div class="col-sm-12">
            <div class="form-group">
                <label for="client_name">Short Name Code</label>
                <input type="text" name="short_code" id="client_name" class="form-control input-xs" value="<?php echo @$client['short_code']?>">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="client_address">Address</label>
                <textarea name="address" id="client_address" rows="2" class="form-control input-xs"><?php echo @$client['address']?></textarea>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="client_vat">VAT Number</label>
                <input type="text" name="vat_no" id="client_vat" class="form-control input-xs" value="<?php echo @$client['vat_no']?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="client_ecc">ECC Number</label>
                <input type="text" name="ecc_no" id="client_ecc" class="form-control input-xs" value="<?php echo @$client['ecc_no']?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="client_lbt">LBT Number</label>
                <input type="text" name="lbt_no" id="client_lbt" class="form-control input-xs" value="<?php echo @$client['lbt_no']?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="client_contact_person">Contact Person</label>
                <input type="text" name="contact_person" id="client_contact_person" class="form-control input-xs" value="<?php echo @$client['contact_person']?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="client_contact_number">Contact Number</label>
                <input type="text" name="contact_no" id="client_contact_number" class="form-control input-xs" value="<?php echo @$client['contact_no']?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="text-center">
                <button class="btn btn-primary btn-xs" type="submit">Submit</button>
                <button class="btn btn-info btn-xs" type="reset">Reset</button>
                <?php if(!isset($_REQUEST['popup'])) {?><button class="btn btn-default btn-xs" type="button" onclick='$("#client").refreshModule();'>Close</button> <?php } ?>
            </div>
        </div>
    </div>
</form>
