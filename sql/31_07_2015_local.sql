-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `_entity`;
CREATE TABLE `_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_entity` (`id`, `name`) VALUES
(1,	'Client'),
(2,	'Vendor'),
(6,	'Project');

DROP TABLE IF EXISTS `_entity_module`;
CREATE TABLE `_entity_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_id` (`module_id`),
  KEY `entity_id` (`entity_id`),
  CONSTRAINT `_entity_module_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `_module` (`id`) ON DELETE CASCADE,
  CONSTRAINT `_entity_module_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_entity_module` (`id`, `module_id`, `entity_id`, `timestamp`) VALUES
(12,	106,	6,	'2015-07-31 07:24:02'),
(26,	110,	1,	'2015-07-31 13:23:50');

DROP TABLE IF EXISTS `_entity_value`;
CREATE TABLE `_entity_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  CONSTRAINT `_entity_value_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `_entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_entity_value` (`id`, `entity_id`, `timestamp`) VALUES
(1,	1,	'2015-07-28 09:22:47'),
(2,	1,	'2015-07-28 09:32:00'),
(3,	2,	'2015-07-28 09:34:40');

DROP TABLE IF EXISTS `_module`;
CREATE TABLE `_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `process_name` varchar(100) NOT NULL,
  `class` varchar(20) NOT NULL DEFAULT 'panel-default',
  `html_id` varchar(30) NOT NULL,
  `movable` tinyint(1) NOT NULL DEFAULT '1',
  `collapsible` tinyint(1) NOT NULL DEFAULT '1',
  `expandable` tinyint(1) NOT NULL DEFAULT '1',
  `no_padding` tinyint(1) NOT NULL,
  `no_header` tinyint(1) NOT NULL DEFAULT '0',
  `extended` tinyint(1) NOT NULL DEFAULT '0',
  `column_number` varchar(5) NOT NULL,
  `sort` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `html_id` (`html_id`,`page_id`),
  KEY `page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_module` (`id`, `title`, `module_name`, `process_name`, `class`, `html_id`, `movable`, `collapsible`, `expandable`, `no_padding`, `no_header`, `extended`, `column_number`, `sort`, `page_id`) VALUES
(7,	'List of Pages',	'super/page-list.module',	'remove-page.process',	'panel-success',	'pages-list',	1,	1,	1,	1,	0,	0,	'2',	2,	3),
(8,	'Module',	'super/module.module',	'module.process',	'panel-primary',	'new-module',	1,	1,	1,	0,	0,	0,	'1',	1,	3),
(9,	'List of Modules',	'super/module-list.module',	'',	'panel-success',	'module-list',	1,	1,	1,	1,	0,	0,	'3',	2,	3),
(10,	'Role/Page Association',	'super/role-page-association.module',	'role-page-association.process',	'panel-info',	'role-page',	1,	1,	1,	0,	0,	0,	'2',	2,	3),
(65,	'Add/Update Page',	'super/page.module',	'page.process',	'panel-success',	'new-page',	1,	1,	1,	0,	0,	0,	'1',	1,	3),
(83,	'Add/Update Role',	'super/role.module',	'role.process',	'panel-info',	'role',	1,	1,	1,	0,	0,	0,	'2',	5,	3),
(84,	'Role List',	'super/role-list.module',	'remove-role.process',	'panel-success',	'role-list',	1,	1,	1,	0,	0,	0,	'3',	5,	3),
(104,	'User Management',	'user/user.module',	'user/user.process',	'',	'user',	1,	1,	1,	0,	1,	0,	'1',	1,	2),
(105,	'Task',	'task/task.module',	'task/task.process',	'',	'task',	1,	1,	1,	0,	1,	0,	'1',	1,	20),
(106,	'Project Management',	'entity/entity.module?entity_module_link=12',	'entity/entity.process?entity_module_link=12',	'',	'project',	1,	1,	1,	0,	1,	0,	'1',	1,	21),
(107,	'Client',	'client/client.module',	'client/client.process',	'',	'client',	1,	1,	1,	0,	0,	0,	'1',	1,	22),
(108,	'Entity',	'entity/entity.module',	'entity/entity.process',	'',	'entity',	1,	1,	1,	0,	0,	0,	'1',	1,	11),
(109,	'Design Module',	'entity/design-module.module',	'entity/design-module.process',	'',	'design-module',	1,	1,	1,	0,	0,	0,	'2',	1,	11),
(110,	'Test',	'entity/entity.module?entity_module_link=26',	'entity/entity.process?entity_module_link=26',	'',	'test',	1,	1,	1,	0,	0,	0,	'1',	3,	11);

DROP TABLE IF EXISTS `_page`;
CREATE TABLE `_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `view` varchar(7) NOT NULL DEFAULT '9:3' COMMENT 'Ratio of two column',
  `icon` varchar(20) NOT NULL COMMENT 'Font Awesome Icon Class',
  `is_single_module` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL COMMENT 'sorting order',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_page` (`id`, `name`, `title`, `view`, `icon`, `is_single_module`, `sort`) VALUES
(2,	'user-management',	'User Management',	'12:0',	'users',	1,	1),
(3,	'access-management',	'Access Management',	'4:4:4',	'lock',	0,	1),
(11,	'entity-management',	'Entity Management',	'6:6',	'cubes',	0,	1),
(20,	'task-management',	'Task Management',	'12:0',	'th-list',	1,	1),
(21,	'project-management',	'Project Management',	'12:0',	'bolt',	1,	1),
(22,	'setup',	'Setup',	'4:4:4',	'gear',	0,	1);

DROP TABLE IF EXISTS `_property`;
CREATE TABLE `_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `type` varchar(50) NOT NULL,
  `entity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  CONSTRAINT `_property_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_property` (`id`, `name`, `display_name`, `hidden`, `type`, `entity_id`) VALUES
(1,	'first_name',	'First name',	0,	'text',	1),
(2,	'last_name',	'Last Name',	0,	'text',	1),
(3,	'address',	'Address',	0,	'text',	1),
(4,	'first_name',	'First name',	0,	'text',	2),
(5,	'last_name',	'Last Name',	0,	'text',	2),
(6,	'address',	'Address',	0,	'text',	2),
(7,	'pan_no',	'Pan Number',	0,	'text',	2),
(14,	'name',	'Name',	0,	'text',	6),
(15,	'client_id',	'Client',	0,	'text',	6);

DROP TABLE IF EXISTS `_property_value`;
CREATE TABLE `_property_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(200) NOT NULL,
  `property_id` int(11) NOT NULL,
  `entity_value_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_id` (`property_id`),
  KEY `entity_value_id` (`entity_value_id`),
  CONSTRAINT `_property_value_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `_property` (`id`) ON DELETE CASCADE,
  CONSTRAINT `_property_value_ibfk_2` FOREIGN KEY (`entity_value_id`) REFERENCES `_entity_value` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_property_value` (`id`, `value`, `property_id`, `entity_value_id`) VALUES
(1,	'Mayank',	1,	1),
(2,	'Valani',	2,	1),
(3,	'Somewhere in Navi Mumbai',	3,	1),
(4,	'Sushant',	1,	2),
(5,	'Rokade',	2,	2),
(6,	'Somewhere in Kanjurmarg',	3,	2),
(7,	'Sushant',	4,	3),
(8,	'Rokade',	5,	3),
(9,	'Somewhere in Kanjurmarg',	6,	3);

DROP TABLE IF EXISTS `_role`;
CREATE TABLE `_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `role_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  `visible` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `_role` (`id`, `role_code`, `role_name`, `enabled`, `visible`) VALUES
(1,	'superadmin',	'Super Admin',	1,	0),
(2,	'admin',	'Administrator',	1,	1),
(3,	'developer',	'Developer',	1,	1);

DROP TABLE IF EXISTS `_role_page`;
CREATE TABLE `_role_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `sort` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id_2` (`role_id`,`page_id`),
  KEY `role_id` (`role_id`,`page_id`),
  KEY `page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `_role_page` (`id`, `role_id`, `page_id`, `sort`) VALUES
(1,	1,	3,	1),
(28,	1,	11,	1),
(60,	3,	20,	1),
(61,	1,	2,	1),
(63,	2,	20,	1),
(64,	2,	21,	1),
(67,	2,	22,	50);

DROP TABLE IF EXISTS `_user`;
CREATE TABLE `_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role` (`role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `_user` (`id`, `username`, `password`, `first_name`, `last_name`, `photo`, `role_id`, `active`, `last_login`) VALUES
(1,	'superadmin',	'a255c308a231abc2fc24d8f9b8627327',	'Super',	'Admin',	'',	1,	1,	'2015-07-31 18:32:56'),
(15,	'kaushil',	'523e6a91ff9d5b8abd9e794aa500d5ef',	'Kaushil',	'Rakhasiya',	'',	2,	1,	'2015-07-31 18:57:17'),
(16,	'sushant.rokade',	'47309bc76913231b94bcf174b3a8dc91',	'Sushant',	'Rokade',	'',	3,	1,	'0000-00-00 00:00:00'),
(17,	'shweta.datar',	'523e6a91ff9d5b8abd9e794aa500d5ef',	'Shweta',	'Datar',	'',	3,	1,	'2015-07-28 10:58:37'),
(18,	'raj',	'523e6a91ff9d5b8abd9e794aa500d5ef',	'Raj',	'Patel',	'',	2,	1,	'0000-00-00 00:00:00'),
(19,	'mayur.patel',	'1856a717782a7e13204c037c9e05bc99',	'Mayur',	'Patel',	'',	3,	1,	'0000-00-00 00:00:00');

-- 2015-07-31 13:37:02
